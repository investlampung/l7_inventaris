<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Auth::routes([
    'register' => false, // Registration Routes...
    'reset' => false, // Password Reset Routes...
    'verify' => false, // Email Verification Routes...
]);

Route::get('/home', 'HomeController@index')->name('home');

//ADMIN
Route::get('/get-data-anggota/{id}', 'Web\Admin\Anggota@getDataAnggota')->name('getData');
Route::get('/get-data-anggota2/{id}', 'Web\Admin\Anggota@getDataAnggota2')->name('getData2');
Route::get('/get-data-barang/{id}', 'Web\Admin\Barang@getDataBarang')->name('getDataBarang');
Route::get('/get-data-barang2/{id}', 'Web\Admin\Barang@getDataBarang2')->name('getDataBarang2');
Route::get('/get-data-barang3/{id}', 'Web\Admin\Barang@getDataBarang3')->name('getDataBarang3');
Route::get('admin-anggota-ajax', 'Web\Admin\Anggota@dataAjax');
//Beranda
Route::resource('admin/beranda', 'Web\Admin\BerandaController', ['as' => 'admin']);
//Data Master
Route::resource('admin/anggota', 'Web\Admin\AnggotaController', ['as' => 'admin']);
Route::get('admin/anggota/hapus/{id}', 'Web\Admin\AnggotaController@hapus');
Route::resource('admin/akun', 'Web\Admin\AkunController', ['as' => 'admin']);
Route::resource('admin/ruang', 'Web\Admin\RuangController', ['as' => 'admin']);
Route::resource('admin/kategori', 'Web\Admin\KategoriController', ['as' => 'admin']);
Route::resource('admin/barang', 'Web\Admin\BarangController', ['as' => 'admin']);
Route::resource('admin/barangdetail', 'Web\Admin\BarangDetailController', ['as' => 'admin']);
//Barang Masuk
Route::resource('admin/keranjangbarangmasuk', 'Web\Admin\BarangMasukKeranjangController', ['as' => 'admin']);
Route::resource('admin/barangmasuk', 'Web\Admin\BarangMasukController', ['as' => 'admin']);
Route::get('/inputmasuk', 'Web\Admin\BarangMasukKeranjangController@input');
//Peminjaman
Route::resource('admin/keranjangpeminjaman', 'Web\Admin\PeminjamanKeranjangController', ['as' => 'admin']);
Route::resource('admin/peminjaman', 'Web\Admin\PeminjamanController', ['as' => 'admin']);
Route::get('/inputpinjam', 'Web\Admin\PeminjamanKeranjangController@input');
Route::post('/admin/keranjangpeminjaman/tampil', 'Web\Admin\PeminjamanKeranjangController@tampil');
Route::get('/admin/peminjaman/status/{id}', 'Web\Admin\PeminjamanController@status');
//Barang Ruangan
Route::resource('admin/keranjangruangan', 'Web\Admin\RuanganKeranjangController', ['as' => 'admin']);
Route::resource('admin/ruangan', 'Web\Admin\RuanganController', ['as' => 'admin']);
Route::post('/admin/keranjangruangan/tampil', 'Web\Admin\RuanganKeranjangController@tampil');
Route::get('/inputruangan', 'Web\Admin\RuanganKeranjangController@input');
Route::get('/admin/ruangan/tampil/{ruang}', 'Web\Admin\RuanganController@tampil');
//Barang Keluar
Route::resource('admin/keranjangkeluar', 'Web\Admin\KeluarKeranjangController', ['as' => 'admin']);
Route::resource('admin/keluar', 'Web\Admin\KeluarController', ['as' => 'admin']);
Route::post('/admin/keranjangkeluar/tampil', 'Web\Admin\KeluarKeranjangController@tampil');
Route::get('/inputkeluar', 'Web\Admin\KeluarKeranjangController@input');
//Barang Rusak Ruangan
Route::resource('admin/keranjangrusakruangan', 'Web\Admin\RusakRuanganKeranjangController', ['as' => 'admin']);
Route::resource('admin/rusakruangan', 'Web\Admin\RusakRuanganController', ['as' => 'admin']);
Route::post('/admin/keranjangrusakruangan/tampil', 'Web\Admin\RusakRuanganKeranjangController@tampil');
Route::get('/inputrusakruangan', 'Web\Admin\RusakRuanganKeranjangController@input');
Route::get('/admin/rusakruangan/status/{id}', 'Web\Admin\RusakRuanganController@status');
//Barang Rusak Ruangan
Route::resource('admin/keranjangbarangrusak', 'Web\Admin\RusakKeranjangController', ['as' => 'admin']);
Route::resource('admin/barangrusak', 'Web\Admin\RusakController', ['as' => 'admin']);
Route::post('/admin/keranjangbarangrusak/tampil', 'Web\Admin\RusakKeranjangController@tampil');
Route::get('/inputbarangrusak', 'Web\Admin\RusakKeranjangController@input');
Route::get('/admin/barangrusak/status/{id}', 'Web\Admin\RusakController@status');


//Sarpras
//Beranda
Route::resource('sarpras/beranda', 'Web\Sarpras\BerandaController', ['as' => 'sarpras']);


//Anggota
//Beranda
Route::resource('anggota/beranda', 'Web\Anggota\BerandaController', ['as' => 'anggota']);
