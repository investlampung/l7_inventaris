<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>MagNews</title>
    @include('layouts.head')

  </head>
  <body>
  {{-- <body class="animsition"> --}}
    @php
      $facebook = App\Model\Meta::find(1);
      $youtube = App\Model\Meta::find(2);
      $twitter = App\Model\Meta::find(3);
      $berita = App\Model\Meta::find(4);
    @endphp

    <header>
      <div class="container-menu-desktop">
        <div class="topbar">
          <div class="content-topbar container h-100">
            <div class="left-topbar">
              <span class="left-topbar-item flex-wr-s-c">
                <span>
                  {{ tanggal_indonesia(now()) }}
                </span>
              </span>
  
              <a href="{{ asset('diskusi') }}" class="left-topbar-item">
                Diskusi
              </a>
              <a href="{{ asset('tentang') }}" class="left-topbar-item">
                Tentang
              </a>
  
              <a href="{{ asset('kontak') }}" class="left-topbar-item">
                Kontak
              </a>
              @guest
                <a href="{{ asset('register') }}" class="left-topbar-item">
                  Daftar
                </a>
    
                <a href="{{ asset('login') }}" class="left-topbar-item">
                  Masuk
                </a>
              @else
                @if( Auth::user()->role=='superadmin' )
                  <a href="{{ asset('superadmin/beranda') }}" class="left-topbar-item">
                    {{ Auth::user()->name }}
                  </a>
                @elseif( Auth::user()->role=='admin' )
                  <a href="{{ asset('admin/beranda') }}" class="left-topbar-item">
                    {{ Auth::user()->name }}
                  </a>
                @else
                  <a href="{{ asset('user/beranda') }}" class="left-topbar-item">
                    {{ Auth::user()->name }}
                  </a>
                @endif
              @endguest
            </div>
  
            <div class="right-topbar">
              @if($facebook->aksi=='T')
                <a href="{{ $facebook->meta_value}}" target="_blank">
                  <span class="fab fa-facebook-f"></span>
                </a>
              @endif
              @if($twitter->aksi=='T')
                <a href="{{ $twitter->meta_value}}" target="_blank">
                  <span class="fab fa-twitter"></span>
                </a>
              @endif
              @if($youtube->aksi=='T')
                <a href="{{ $youtube->meta_value}}" target="_blank">
                  <span class="fab fa-youtube"></span>
                </a>
              @endif
            </div>
          </div>
        </div>
  
        <!-- Header Mobile -->
        <div class="wrap-header-mobile">
          <div class="logo-mobile">
            <a href="{{ asset('/') }}"><img src="{{ asset('itlabil/user/images/icons/logo-01.png') }}" alt="IMG-LOGO"></a>
          </div>
  
          <div class="btn-show-menu-mobile hamburger hamburger--squeeze m-r--8">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
            </span>
          </div>
        </div>
  
        <!-- Menu Mobile -->
        <div class="menu-mobile">
          <ul class="topbar-mobile">
            <li class="left-topbar">
              <span class="left-topbar-item flex-wr-s-c">
                <span>
                  {{ tanggal_indonesia(now()) }}
                </span>
              </span>
            </li>
  
            <li class="left-topbar">
              <a href="{{ asset('diskusi') }}" class="left-topbar-item">
                Diskusi
              </a>
              <a href="{{ asset('tentang') }}" class="left-topbar-item">
                Tentang
              </a>
  
              <a href="{{ asset('kontak') }}" class="left-topbar-item">
                Kontak
              </a>
  
              @guest
                <a href="{{ asset('register') }}" class="left-topbar-item">
                  Daftar
                </a>
    
                <a href="{{ asset('login') }}" class="left-topbar-item">
                  Masuk
                </a>
              @else
                @if( Auth::user()->role=='superadmin' )
                  <a href="{{ asset('superadmin/beranda') }}" class="left-topbar-item">
                    {{ Auth::user()->name }}
                  </a>
                @elseif( Auth::user()->role=='admin' )
                  <a href="{{ asset('admin/beranda') }}" class="left-topbar-item">
                    {{ Auth::user()->name }}
                  </a>
                @else
                  <a href="{{ asset('user/beranda') }}" class="left-topbar-item">
                    {{ Auth::user()->name }}
                  </a>
                @endif
              @endguest
            </li>
  
            <li class="right-topbar">
              @if($facebook->aksi=='T')
                <a href="{{ $facebook->meta_value}}" target="_blank">
                  <span class="fab fa-facebook-f"></span>
                </a>
              @endif
              @if($twitter->aksi=='T')
                <a href="{{ $twitter->meta_value}}" target="_blank">
                  <span class="fab fa-twitter"></span>
                </a>
              @endif
              @if($youtube->aksi=='T')
                <a href="{{ $youtube->meta_value}}" target="_blank">
                  <span class="fab fa-youtube"></span>
                </a>
              @endif
            </li>
          </ul>
  
          <ul class="main-menu-m">
            <li>
              <a href="{{ asset('/') }}">Home</a>
            </li>
  
            @foreach(App\Model\Kategori::select('kategori')->get()->all() as $all_tahun)
              <li>
                <a href="{{ asset('kategori/') }}/{{ $all_tahun->kategori }}">{{ $all_tahun->kategori }}</a>
              </li>
            @endforeach
          </ul>
        </div>
        
        <!--  -->
        <div class="wrap-logo container">
          <!-- Logo desktop -->		
          <div class="logo">
            <a href="{{ asset('/') }}"><img src="{{ asset('itlabil/user/images/icons/logo-01.png') }}" alt="LOGO"></a>
          </div>	
  
          <!-- Banner -->
          <div class="banner-header">
            {{-- <a href="https://themewagon.com/themes/free-bootstrap-4-html5-news-website-template-magnews2/"><img src="images/banner-01.jpg" alt="IMG"></a> --}}
          </div>
        </div>	
        
        <!--  -->
        <div class="wrap-main-nav">
          <div class="main-nav">
            <!-- Menu desktop -->
            <nav class="menu-desktop">
              <a class="logo-stick" href="{{ asset('/') }}">
                <img src="{{ asset('itlabil/user/images/icons/logo-01.png') }}" alt="LOGO">
              </a>
  
              <ul class="main-menu">
                <li class="main-menu-active">
                  <a href="{{ asset('/') }}">Home</a>
                </li>
  
                @foreach(App\Model\Kategori::get()->all() as $kategori)
                  <li class="mega-menu-item">
                    <a href="{{ asset('kategori/') }}/{{ $kategori->kategori }}">{{ $kategori->kategori }}</a>
    
                    <div class="sub-mega-menu">
    
                      <div class="tab-content">
                        <div class="tab-pane show active" id="news-0" role="tabpanel">
                          <div class="row">
                            @foreach(App\Model\Berita::where('kategori_id', $kategori->id )->orderBy('id', 'desc')->take(4)->get() as $beritaaa)
                              <div class="col-3">
                                <!-- Item post -->	
                                <div>
                                  <a href="blog-detail-01.html" class="wrap-pic-w hov1 trans-03">
                                    <img src="{{ asset('itlabil/image/berita')}}/{{ $beritaaa->photo }}" alt="IMG">
                                  </a>
      
                                  <div class="p-t-10">
                                    <h5 class="p-b-5">
                                      <a href="{{ asset('berita/') }}/{{ $beritaaa->id }}" class="f1-s-5 cl3 hov-cl10 trans-03">
                                        {{ Str::limit($beritaaa->judul,40) }}
                                      </a>
                                    </h5>
      
                                    <span class="cl8">
                                      <a href="#" class="f1-s-6 cl8 hov-cl10 trans-03">
                                        {{ $beritaaa->user->name }}
                                      </a>
      
                                      <span class="f1-s-3 m-rl-3">
                                        -
                                      </span>
      
                                      <span class="f1-s-3">
                                        {{ tanggal_local($beritaaa->created_at) }}
                                      </span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            @endforeach
                          </div>
                        </div>

                      </div>
                    </div>
                  </li>
                @endforeach

              </ul>
            </nav>
          </div>
        </div>	
      </div>
    </header>

    @yield('content')

    <footer>
      <div class="bg2 p-t-40 p-b-25">
        <div class="container">
          <div class="row">
            <div class="col-lg-4 p-b-20">
              <div class="size-h-3 flex-s-c">
                <a href="index.html">
                  <img class="max-s-full" src="{{ asset('itlabil/user/images/icons/logo-02.png') }}" alt="LOGO">
                </a>
              </div>
  
              <div>
                <p class="f1-s-1 cl11 p-b-16">
                  {{ $berita->meta_value }}
                </p>
  
                <div class="p-t-15">
                  @if($facebook->aksi=='T')
                    <a href="{{ $facebook->meta_value}}" class="fs-18 cl11 hov-cl10 trans-03 m-r-8" target="_blank">
                      <span class="fab fa-facebook-f"></span>
                    </a>
                  @endif
                  
                  @if($twitter->aksi=='T')
                    <a href="{{ $twitter->meta_value}}" class="fs-18 cl11 hov-cl10 trans-03 m-r-8" target="_blank">
                      <span class="fab fa-twitter"></span>
                    </a>
                  @endif
                  @if($youtube->aksi=='T')
                    <a href="{{ $youtube->meta_value}}" class="fs-18 cl11 hov-cl10 trans-03 m-r-8" target="_blank">
                      <span class="fab fa-youtube"></span>
                    </a>
                  @endif
                </div>
              </div>
            </div>
  
            <div class="col-sm-6 col-lg-4 p-b-20">
              <div class="size-h-3 flex-s-c">
                <h5 class="f1-m-7 cl0">
                  Recent Post
                </h5>
              </div>
  
              <ul>
                @foreach(App\Model\Berita::orderBy('id', 'desc')->take(3)->get(); as $recentpost)
                  <li class="flex-wr-sb-s p-b-20">
                    <a href="#" class="size-w-4 wrap-pic-w hov1 trans-03">
                      <img src="{{ asset('itlabil/image/berita')}}/{{ $recentpost->photo }}" alt="IMG">
                    </a>
    
                    <div class="size-w-5">
                      <h6 class="p-b-5">
                        <a href="#" class="f1-s-5 cl11 hov-cl10 trans-03">
                          {{ Str::limit($recentpost->judul,30) }}
                        </a>
                      </h6>
    
                      <span class="f1-s-3 cl6">
                        {{ tanggal_indonesia($beritaaa->created_at) }}
                      </span>
                    </div>
                  </li>
                @endforeach

              </ul>
            </div>
  
            <div class="col-sm-6 col-lg-4 p-b-20">
              <div class="size-h-3 flex-s-c">
                <h5 class="f1-m-7 cl0">
                  Category
                </h5>
              </div>
  
              <ul class="m-t--12">
                @foreach(App\Model\Kategori::take(5)->get() as $kateg)
                  <li class="how-bor1 p-rl-5 p-tb-10">
                    <a href="{{ asset('kategori/') }}/{{ $kateg->kategori }}" class="f1-s-5 cl11 hov-cl10 trans-03 p-tb-8">
                      {{ $kateg->kategori }}
                    </a>
                  </li>
                @endforeach
                
              </ul>
            </div>
          </div>
        </div>
      </div>
  
      <div class="bg11">
        <div class="container size-h-4 flex-c-c p-tb-15">
          <span class="f1-s-1 cl0 txt-center">
            Copyright © 2021 Berita
          </span>
        </div>
      </div>
    </footer>
  
    <div class="btn-back-to-top" id="myBtn">
      <span class="symbol-btn-back-to-top">
        <span class="fas fa-angle-up"></span>
      </span>
    </div>
  
    <!-- Modal Video 01-->
    <div class="modal fade" id="modal-video-01" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document" data-dismiss="modal">
        <div class="close-mo-video-01 trans-0-4" data-dismiss="modal" aria-label="Close">&times;</div>
  
        <div class="wrap-video-mo-01">
          <div class="video-mo-01">
            <iframe src="https://www.youtube.com/embed/wJnBTPUQS5A?rel=0" allowfullscreen></iframe>
          </div>
        </div>
      </div>
    </div>
  
    @include('layouts.script')

  </body>
  </html>