<!DOCTYPE html>
<html lang="id">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/png" href="{{ asset('itlabil/image/default/logo.png')}}"/>
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- CSS -->

    <link href="{{ asset('itlabil/admin/bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/Ionicons/css/ionicons.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/dist/css/AdminLTE.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/dist/css/skins/_all-skins.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/morris.js/morris.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/jvectormap/jquery-jvectormap.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

    <link rel="stylesheet" href="{{ asset('itlabil/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link href="{{ asset('itlabil/admin/toast/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('itlabil/admin/dist/css/editsendiri.css') }}" rel="stylesheet">
    <script src="{{ asset('itlabil/admin/other/code/highcharts.js') }}"></script>
</head>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <!-- HEADER -->
        @include('layouts.header')

        <!-- MENU SIDE BAR -->
        <aside class="main-sidebar">
            <section class="sidebar">
                <ul class="sidebar-menu" data-widget="tree">
                    @if(Auth::user()->role==='admin')
                    @include('layouts.menu_admin')
                    @elseif(Auth::user()->role==='sarpras')
                    @include('layouts.menu_sarpras')
                    @else
                    @include('layouts.menu_anggota')
                    @endif
                </ul>
            </section>
        </aside>

        <!-- CONTENT -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                @yield('content')
            </section>
        </div>

        <!-- FOOTER -->
        <footer class="main-footer">
            <strong>Copyright &copy; 2020 SMK Pelita Pesawaran
        </footer>

        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <!-- JS -->
    <script src="{{ asset('itlabil/admin/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
    <script>
        $.widget.bridge('uibutton', $.ui.button);
    </script>
    <script src="{{ asset('itlabil/admin/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/chart.js/Chart.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/raphael/raphael.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/morris.js/morris.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/moment/min/moment.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/fastclick/lib/fastclick.js') }}"></script>
    <script src="{{ asset('itlabil/admin/dist/js/adminlte.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/dist/js/pages/dashboard.js') }}"></script>
    <script src="{{ asset('itlabil/admin/dist/js/demo.js') }}"></script>


    <!-- DataTables -->
    <script src="{{ asset('itlabil/admin/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('itlabil/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

    <script type="text/javascript" src="{{ asset('itlabil/admin/toast/toastr.min.js') }}"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
    <script src="https://code.highcharts.com/modules/export-data.js"></script>
    <script src="https://code.highcharts.com/modules/accessibility.js"></script>

    <script>
        @if(Session::has('message'))
        var type = "{{Session::get('alert-type','info')}}"

        switch (type) {
            case 'info':
                toastr.info("{{ Session::get('message') }}");
                break;
            case 'success':
                toastr.success("{{ Session::get('message') }}");
                break;
            case 'warning':
                toastr.warning("{{ Session::get('message') }}");
                break;
            case 'error':
                toastr.error("{{ Session::get('message') }}");
                break;
        }
        @endif
    </script>

    <script>
        $(function() {
            $('#example1').DataTable()
            $('#example2').DataTable()
            $('#example3').DataTable({
                'paging': true,
                'lengthChange': false,
                'searching': false,
                'ordering': true,
                'info': true,
                'autoWidth': false
            })
        })
    </script>

    <script>
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    $('#category-img-tag').attr('src', e.target.result);
                }

                reader.readAsDataURL(input.files[0]);
            }
        }

        $("#cat_image").change(function() {
            readURL(this);
        });
    </script>
    @yield('script')
    @stack('scripts')
</body>

</html>