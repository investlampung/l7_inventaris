<header class="main-header">
    <a href="{{ asset('admin/beranda') }}" class="logo">
        <span class="logo-mini">Inventaris</span>
        <span class="logo-lg"><b>Inventaris</b></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">

                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        @if(Auth::user()->role==='admin')
                            <img src="{{ asset('itlabil/image/default/avatar4.png') }}" class="user-image" alt="User Image">
                        @else
                            @if(Auth::user()->photo=='-')
                                @if(Auth::user()->jk=='Laki-Laki')
                                    <img src="{{ asset('itlabil/image/default/avatar5.png') }}" class="user-image" alt="User Image">
                                @else
                                    <img src="{{ asset('itlabil/image/default/avatar2.png') }}" class="user-image" alt="User Image">
                                @endif
                            @else
                                <img src="{{ asset('itlabil/image/pp/') }}/{{ Auth::user()->photo }}" class="user-image" alt="User Image">
                            @endif
                        @endif
                        <span class="hidden-xs">{{Auth::user()->name}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            @if(Auth::user()->role==='admin')
                                <img src="{{ asset('itlabil/image/default/avatar4.png') }}" class="img-circle" alt="User Image">
                            @else
                                @if(Auth::user()->photo=='-')
                                    @if(Auth::user()->jk=='Laki-Laki')
                                        <img src="{{ asset('itlabil/image/default/avatar5.png') }}" class="img-circle" alt="User Image">
                                    @else
                                        <img src="{{ asset('itlabil/image/default/avatar2.png') }}" class="img-circle" alt="User Image">
                                    @endif
                                @else
                                    <img src="{{ asset('itlabil/image/pp/') }}/{{ Auth::user()->photo }}" class="img-circle" alt="User Image">
                                @endif
                            @endif
                            <p>{{Auth::user()->name}}</p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div align="center">
                                <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>