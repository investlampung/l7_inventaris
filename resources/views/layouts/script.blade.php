
<!-- JS -->
<script src="{{ asset('itlabil/user/vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('itlabil/user/vendor/animsition/js/animsition.min.js') }}"></script>
<script src="{{ asset('itlabil/user/vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('itlabil/user/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('itlabil/user/js/main.js') }}"></script>

<script type="text/javascript" src="{{ asset('itlabil/admin/toast/toastr.min.js') }}"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/js/select2.min.js"></script>
<script type="text/javascript">
  $('.myselect').select2();
</script>
<script>
    @if(Session::has('message'))
    var type = "{{Session::get('alert-type','info')}}"

    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('message') }}");
            break;
    }
    @endif
</script>