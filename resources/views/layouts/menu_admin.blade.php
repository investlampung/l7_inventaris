<li><a href="{{ asset('admin/beranda') }}"><i class="fa fa-home"></i> <span>Beranda</span></a></li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-database"></i>
        <span>Data Master</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ asset('admin/anggota') }}"><i class="fa fa-circle-o"></i> Anggota</a></li>
        <li><a href="{{ asset('admin/ruang') }}"><i class="fa fa-circle-o"></i> Ruangan</a></li>
        <li><a href="{{ asset('admin/kategori') }}"><i class="fa fa-circle-o"></i> Kategori</a></li>
        <li><a href="{{ asset('admin/barang') }}"><i class="fa fa-circle-o"></i> Barang</a></li>
        <li><a href="{{ asset('admin/akun') }}"><i class="fa fa-circle-o"></i> Akun</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-check-square-o"></i>
        <span>Barang Masuk</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ asset('admin/keranjangbarangmasuk') }}"><i class="fa fa-circle-o"></i> Keranjang Barang Masuk</a></li>
        <li><a href="{{ asset('admin/barangmasuk') }}"><i class="fa fa-circle-o"></i> Data Barang Masuk</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-briefcase"></i>
        <span>Peminjaman</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ asset('admin/keranjangpeminjaman') }}"><i class="fa fa-circle-o"></i> Keranjang Peminjaman</a></li>
        <li><a href="{{ asset('admin/peminjaman') }}"><i class="fa fa-circle-o"></i> Data Peminjaman</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-university"></i>
        <span>Barang Ruangan</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ asset('admin/keranjangruangan') }}"><i class="fa fa-circle-o"></i>Keranjang Barang Ruangan</a></li>
        <li><a href="{{ asset('admin/ruangan') }}"><i class="fa fa-circle-o"></i>Data Barang Ruangan</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-paper-plane"></i>
        <span>Barang Keluar</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ asset('admin/keranjangkeluar') }}"><i class="fa fa-circle-o"></i>Keranjang Barang Keluar</a></li>
        <li><a href="{{ asset('admin/keluar') }}"><i class="fa fa-circle-o"></i>Data Barang Keluar</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-tasks"></i>
        <span>Barang Rusak</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ asset('admin/keranjangbarangrusak') }}"><i class="fa fa-circle-o"></i>Keranjang Barang Rusak</a></li>
        <li><a href="{{ asset('admin/barangrusak') }}"><i class="fa fa-circle-o"></i>Data Barang Rusak</a></li>
    </ul>
</li>
<li class="treeview">
    <a href="#">
        <i class="fa fa-suitcase"></i>
        <span>Barang Rusak Ruangan</span>
        <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
        </span>
    </a>
    <ul class="treeview-menu">
        <li><a href="{{ asset('admin/keranjangrusakruangan') }}"><i class="fa fa-circle-o"></i>Keranjang Rusak Ruangan</a></li>
        <li><a href="{{ asset('admin/rusakruangan') }}"><i class="fa fa-circle-o"></i>Data Rusak Ruangan</a></li>
    </ul>
</li>
{{-- <li><a href="{{ asset('admin/absensi') }}"><i class="fa fa-calendar"></i> <span>Absensi</span></a></li> --}}
