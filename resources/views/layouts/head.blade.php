<link rel="icon" type="image/png" href="{{ asset('itlabil/user/images/icons/favicon.png')}}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/user/vendor/bootstrap/css/bootstrap.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/user/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/user/fonts/fontawesome-5.0.8/css/fontawesome-all.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/user/fonts/iconic/css/material-design-iconic-font.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/user/vendor/animate/animate.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/user/vendor/css-hamburgers/hamburgers.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/user/vendor/animsition/css/animsition.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/user/css/util.min.css')}}">
<link rel="stylesheet" type="text/css" href="{{ asset('itlabil/user/css/main.css')}}">
<link href="{{ asset('itlabil/admin/toast/toastr.min.css') }}" rel="stylesheet">
<link href="https://cdn.jsdelivr.net/npm/select2@4.0.12/dist/css/select2.min.css" rel="stylesheet" />