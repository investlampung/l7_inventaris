@extends('layouts.admin')
@section('title', 'Keranjang Peminjaman')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Keranjang Peminjaman
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Keranjang Peminjaman</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.keranjangpeminjaman.update', $data->id) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama Peminjam</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ $data->user->name }}" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Barang</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ $data->barang_detail->barang->barang }} {{ $data->barang_detail->merk }}" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="number" value="{{ $data->jumlah_pinjam }}" name="jumlah_pinjam" class="form-control" placeholder="{{ $data->jumlah_pinjam+$data->barang_detail->jumlah }}">
                                <small class="text-danger">{{ $errors->first('jumlah_pinjam') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tanggal Pinjam</label>
                            <div class="col-sm-10">
                                <input type="date" name="tanggal_pinjam" value="{{ $data->tanggal_pinjam->format('Y-m-d') }}" class="form-control" placeholder="Tanggal Masuk">
                                <small class="text-danger">{{ $errors->first('tanggal_pinjam') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tanggal Kembali</label>
                            <div class="col-sm-10">
                                <input type="date" name="tanggal_kembali" value="{{ $data->tanggal_kembali->format('Y-m-d') }}" class="form-control" placeholder="Tanggal Masuk">
                                <small class="text-danger">{{ $errors->first('tanggal_kembali') }}</small>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/keranjangbarangmasuk')}}" class="btn btn-default pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">

        function sum() {
            var txtFirstNumberValue = document.getElementById('txt1').value;
            var txtSecondNumberValue = document.getElementById('txt2').value;
            var result = parseFloat(txtFirstNumberValue) * parseFloat(txtSecondNumberValue);
            if (!isNaN(result)) {
                document.getElementById('txt3').value = result;
            }
        }

    </script>

@endpush
@endsection