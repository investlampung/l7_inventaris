@extends('layouts.admin')
@section('title', 'Peminjaman')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Peminjaman
        </h1>
    </section><br><br>

    <div class="row">
        
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Peminjaman</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Peminjam</th>
                                <th>Barang</th>
                                <th>Jumlah</th>
                                <th>Tanggal Pinjam</th>
                                <th>Tanggal Kembali</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->barang_detail->barang->barang }} {{ $item->barang_detail->merk }}</td>
                                <td>{{ $item->jumlah_pinjam }}</td>
                                <td>{{ $item->tanggal_pinjam->format('d-m-Y')}}</td>
                                <td>{{ $item->tanggal_kembali->format('d-m-Y')}}</td>
                                <td>
                                    @if($item->status==='Belum dikembalikan')
                                        <span class="label label-warning">{{ $item->status }}</span>
                                    @else
                                        <span class="label label-success">{{ $item->status }}</span>
                                    @endif
                                </td>
                                <td align="center" width="200px">
                                    @if($item->status==='Belum dikembalikan')
                                        <a class="btn btn-primary" href="{{ route('admin.peminjaman.show',$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i></a>
                                        <a href="/admin/peminjaman/status/{{ $item->id }}" class="btn btn-success"  onclick="return confirm('Apakah Anda Yakin ?')"><i class="fa fa-check"></i></a>
                                    @else
                                        <a class="btn btn-primary" href="{{ route('admin.peminjaman.show',$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection