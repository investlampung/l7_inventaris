@extends('layouts.admin')
@section('title', 'Peminjaman')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Peminjaman
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Peminjaman</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <div class="col-md-8">
                        <table id="example1" class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <td>Nama Peminjam</td>
                                    <td>{{$data->user->name}}</td>
                                </tr>
                                <tr>
                                    <td>Barang</td>
                                    <td>{{$data->barang_detail->barang->barang}}</td>
                                </tr>
                                <tr>
                                    <td>Merk</td>
                                    <td>{{$data->barang_detail->merk}}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah Pinjam</td>
                                    <td>{{$data->jumlah_pinjam}}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Pinjam</td>
                                    <td>{{$data->tanggal_pinjam->format('d-m-Y')}}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Kembali</td>
                                    <td>{{$data->tanggal_kembali->format('d-m-Y')}}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>
                                        @if($data->status==='Belum dikembalikan')
                                            <span class="label label-warning">{{ $data->status }}</span>
                                        @else
                                            <span class="label label-success">{{ $data->status }}</span>
                                        @endif
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @php
                        $harga = "Rp. ".number_format($data->barang_detail->harga_satuan, 0, ".", ".").",-";
                        $nama_barang = $data->barang_detail->barang->barang;
                        $tanggal = $data->barang_detail->tanggal_masuk->format('d-m-Y');
                        $kdbarang = $data->barang_detail->kode_barang;
                        $merk = $data->barang_detail->merk;
                        $sd = $data->barang_detail->sumber_dana;
                        $kt = $data->barang_detail->keterangan;
                        $jumlah = $data->barang_detail->jumlah;
                    @endphp
                    <div class="col-md-4" align="center">
                        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')
                                // ->merge('itlabil/image/default/logo.png', 0.5, true)
                                ->size(250)->errorCorrection('H')
                                ->generate("$kdbarang\nNama : $nama_barang\nMerk : $merk\nJumlah : $jumlah\nHarga : $harga\nSumber Dana : $sd\nTanggal Masuk : $tanggal\nKeterangan : $kt")) !!} ">
                    </div>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <a href="{{url('admin/peminjaman')}}" class="btn btn-default">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection