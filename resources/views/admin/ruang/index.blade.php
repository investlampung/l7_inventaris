@extends('layouts.admin')
@section('title', 'Ruang')
@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Ruang
        </h1>
    </section><br><br>

    <div class="row">
        
        <div class="col-md-4">
            <a href="{{ route('admin.ruang.create') }}" class="btn btn-primary">+ Ruang</a><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Ruang</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Ruang</th>
                                <th>Penanggung Jawab</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->ruang}}</td>
                                <td>{{ $item->user->name}}</td>
                                <td align="center" width="200px">
                                    @if($item->ruangan->count()===0)
                                        <form action="{{ route('admin.ruang.destroy',$item->id) }}" method="POST">
                                            <a class="btn btn-success" href="{{ route('admin.ruang.edit',$item->id) }}" alt="Edit"><i class="fa fa-edit"></i></a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger" alt="Hapus"><i class="fa fa-trash"></i></button>
                                        </form>
                                    @else
                                        <a class="btn btn-primary" href="{{ route('admin.ruang.show',$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-success" href="{{ route('admin.ruang.edit',$item->id) }}" alt="Edit"><i class="fa fa-edit"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection