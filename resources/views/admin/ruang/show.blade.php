@extends('layouts.admin')
@section('title', 'Barang Ruangan')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Barang Ruangan {{ $data->ruang }}
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Barang</th>
                                <th>Jumlah</th>
                                <th>Jumlah Rusak</th>
                                <th>Tanggal Masuk</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data->ruangan as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->barang_detail->barang->barang }} {{ $item->barang_detail->merk }}</td>
                                <td>{{ $item->jumlah }}</td>
                                <td>{{ $item->jumlah_rusak }}</td>
                                <td>{{ $item->tanggal_masuk->format('d-m-Y')}}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection