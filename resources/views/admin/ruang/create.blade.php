@extends('layouts.admin')
@section('title', 'Ruang')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Ruang
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Ruang</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.ruang.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Ruang</label>
                            <div class="col-sm-10">
                                <input type="text" name="ruang" class="form-control" placeholder="Ruang : Ex: Lab TKJ">
                                <small class="text-danger">{{ $errors->first('ruang') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tahun" class="col-sm-2 control-label">P. Jawab</label>

                            <div class="col-sm-10">
                                <select class="form-control" name="user_id">
                                    @foreach ($anggota as $item)
                                        <option value="{{ $item->id }}">{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/ruang')}}" class="btn btn-default  pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection