@extends('layouts.admin')
@section('title', 'Keranjang Barang Masuk')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Keranjang Barang Masuk
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Keranjang Barang Masuk</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.keranjangbarangmasuk.update', $data->id) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Kode Barang</label>
                            <div class="col-sm-10">
                                <input type="text" name="kode_barang" value="{{ $data->kode_barang }}" readonly class="form-control" placeholder="Jumlah">
                                <small class="text-danger">{{ $errors->first('jumlah') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tahun" class="col-sm-2 control-label">Barang</label>

                            <div class="col-sm-10">
                                <select class="form-control" name="barang_id">
                                    <option value="{{ $data->barang_id }}">{{ $data->barang->barang}}</option>
                                    @foreach ($barang as $item)
                                        <option value="{{ $item->id }}">{{ $item->barang}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="number" value="{{ $data->jumlah }}" name="jumlah" id="txt1" onkeyup="sum();" class="form-control" placeholder="Jumlah">
                                <small class="text-danger">{{ $errors->first('jumlah') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Harga Satuan</label>
                            <div class="col-sm-10">
                                <input type="number" value="{{ $data->harga_satuan }}" name="harga_satuan" id="txt2" onkeyup="sum();" class="form-control" placeholder="Harga Satuan">
                                <small class="text-danger">{{ $errors->first('harga_satuan') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Harga Total</label>
                            <div class="col-sm-10">
                                <input type="number" value="{{ $data->harga_total }}" name="harga_total" id="txt3" readonly class="form-control" placeholder="Harga Total">
                                <small class="text-danger">{{ $errors->first('harga_total') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Merk</label>
                            <div class="col-sm-10">
                                <input type="text" name="merk" value="{{ $data->merk }}" class="form-control" placeholder="Merk">
                                <small class="text-danger">{{ $errors->first('merk') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Sumber Dana</label>
                            <div class="col-sm-10">
                                <input type="text" name="sumber_dana" value="{{ $data->sumber_dana }}" class="form-control" placeholder="Sumber Dana">
                                <small class="text-danger">{{ $errors->first('sumber_dana') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tanggal Masuk</label>
                            <div class="col-sm-10">
                                <input type="date" name="tanggal_masuk" value="{{ $data->tanggal_masuk->format('Y-m-d') }}" class="form-control" placeholder="Tanggal Masuk">
                                <small class="text-danger">{{ $errors->first('tanggal_masuk') }}</small>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="tahun" class="col-sm-2 control-label">Keterangan</label>

                            <div class="col-sm-10">
                                <div class="box-body pad">
                                    <textarea class="textarea" name="keterangan" placeholder="Keterangan" id="isi" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px">{{ $data->keterangan }}</textarea>
                                </div>
                                <small class="text-danger">{{ $errors->first('keterangan') }}</small>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/keranjangbarangmasuk')}}" class="btn btn-default pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">

        function sum() {
            var txtFirstNumberValue = document.getElementById('txt1').value;
            var txtSecondNumberValue = document.getElementById('txt2').value;
            var result = parseFloat(txtFirstNumberValue) * parseFloat(txtSecondNumberValue);
            if (!isNaN(result)) {
                document.getElementById('txt3').value = result;
            }
        }

    </script>

@endpush
@endsection