@extends('layouts.admin')
@section('title', 'Barang Masuk')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Barang Masuk
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Barang - {{$data->barang->barang}}</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <div class="col-md-8">
                        <table id="example1" class="table table-bordered table-striped">
                            <tbody>
                                <tr>
                                    <td>Kode Barang</td>
                                    <td>{{$data->kode_barang}}</td>
                                </tr>
                                <tr>
                                    <td>Nama Barang</td>
                                    <td>{{$data->barang->barang}}</td>
                                </tr>
                                <tr>
                                    <td>Merk</td>
                                    <td>{{$data->merk}}</td>
                                </tr>
                                <tr>
                                    <td>Jumlah</td>
                                    <td>{{$data->jumlah}}</td>
                                </tr>
                                <tr>
                                    <td>Harga Satuan</td>
                                    <td>Rp. {{ number_format($data->harga_satuan, 0, ".", ".")}},-</td>
                                </tr>
                                <tr>
                                    <td>Total</td>
                                    <td>Rp. {{ number_format($data->harga_total, 0, ".", ".")}},-</td>
                                </tr>
                                <tr>
                                    <td>Sumber Dana</td>
                                    <td>{{$data->sumber_dana}}</td>
                                </tr>
                                <tr>
                                    <td>Tanggal Masuk</td>
                                    <td>{{$data->tanggal_masuk->format('d-m-Y')}}</td>
                                </tr>
                                <tr>
                                    <td>Keterangan</td>
                                    <td>{!! $data->keterangan !!}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    @php
                        $harga = "Rp. ".number_format($data->harga_satuan, 0, ".", ".").",-";
                        $nama_barang = $data->barang->barang;
                        $tanggal = $data->tanggal_masuk->format('d-m-Y');
                    @endphp
                    <div class="col-md-4" align="center">
                        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')
                                // ->merge('itlabil/image/default/logo.png', 0.5, true)
                                ->size(250)->errorCorrection('H')
                                ->generate("$data->kode_barang\nNama : $nama_barang\nMerk : $data->merk\nJumlah : $data->jumlah\nHarga : $harga\nSumber Dana : $data->sumber_dana\nTanggal Masuk : $tanggal\nKeterangan : $data->keterangan")) !!} ">
                    </div>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <a href="{{url('admin/barangmasuk')}}" class="btn btn-default">Kembali</a>
                    <a class="btn btn-success" href="{{ route('admin.barangmasuk.edit',$data->id) }}">Ubah</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection