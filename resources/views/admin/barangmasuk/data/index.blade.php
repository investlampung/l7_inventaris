@extends('layouts.admin')
@section('title', 'Barang Masuk')
@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Barang Masuk
        </h1>
    </section><br><br>

    <div class="row">
        
        <div class="col-md-4">
            <a href="{{ route('admin.barangmasuk.create') }}" class="btn btn-primary">+ Barang Masuk</a><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Barang Masuk</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Barang</th>
                                <th>Merk</th>
                                <th>Jumlah</th>
                                <th>Harga Satuan</th>
                                <th>Tanggal Masuk</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->kode_barang }}</td>
                                <td>{{ $item->barang->barang }}</td>
                                <td>{{ $item->merk }}</td>
                                <td>{{ $item->jumlah }}</td>
                                <td>Rp. {{ number_format($item->harga_satuan, 0, ".", ".") }},-</td>
                                <td>{{ $item->tanggal_masuk->format('d-m-Y') }}</td>
                                <td align="center" width="200px">
                                    <a class="btn btn-primary" href="{{ route('admin.barangmasuk.show',$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i></a>
                                    <a class="btn btn-success" href="{{ route('admin.barangmasuk.edit',$item->id) }}" alt="Edit"><i class="fa fa-edit"></i></a>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection