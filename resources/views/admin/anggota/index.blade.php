@extends('layouts.admin')
@section('title', 'Anggota')
@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Anggota
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-4">
            <a href="{{ route('admin.anggota.create') }}" class="btn btn-primary">+ Anggota</a><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Anggota</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>No Induk</th>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>Telp</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->no_induk}}</td>
                                <td>{{ $item->name}}</td>
                                <td>{{ $item->email}}</td>
                                <td>{{ $item->no_telp}}</td>
                                <td>{{ $item->status}}</td>
                                <td align="center" width="200px">
                                    <a class="btn btn-primary" href="{{ route('admin.anggota.show',$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i></a>
                                    <a class="btn btn-success" href="{{ route('admin.anggota.edit',$item->id) }}" alt="Edit"><i class="fa fa-edit"></i></a>
                                    <a class="btn btn-danger" href="{{ url('admin/anggota/hapus'.'/'.$item->id) }}" alt="Hapus"><i class="fa fa-trash"></i></a>

                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection