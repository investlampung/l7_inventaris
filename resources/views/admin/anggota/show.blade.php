@extends('layouts.admin')
@section('title', 'Anggota')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Anggota
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">{{$data->name}}</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <div class="col-md-8">
                        <table id="example1" class="table table-bordered table-striped">
                            <tbody>
                                @if($data->photo=='-')
                                @else
                                <tr>
                                    <td colspan="2" align="center">
                                        <img src="{{ asset('itlabil/image/pp') }}/{{ $data->photo }}" id="category-img-tag" alt="" style="height: 200px;width:200px;object-fit:cover" class="img-thumbnail">
                                    </td>
                                </tr>
                                @endif
                                
                                <tr>
                                    <td>No Induk</td>
                                    <td>{{$data->no_induk}}</td>
                                </tr>
                                <tr>
                                    <td>Nama</td>
                                    <td>{{$data->name}}</td>
                                </tr>
                                <tr>
                                    <td>Email</td>
                                    <td>{{$data->email}}</td>
                                </tr>
                                <tr>
                                    <td>JK</td>
                                    <td>{{$data->jk}}</td>
                                </tr>
                                <tr>
                                    <td>No Telp</td>
                                    <td>{{$data->no_telp}}</td>
                                </tr>
                                <tr>
                                    <td>Status</td>
                                    <td>{{$data->status}}</td>
                                </tr>
                                <tr>
                                    <td>Alamat</td>
                                    <td>{!! $data->alamat !!}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="col-md-4" align="center">
                        <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')
                                ->size(250)->errorCorrection('H')
                                ->generate("$data->no_induk\nNama : $data->name\nEmail : $data->email\nNo Telp : $data->no_telp\nStatus : $data->status\nAlamat : $data->alamat")) !!} ">
                    </div>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <a href="{{url('admin/anggota')}}" class="btn btn-default">Kembali</a>
                    <a class="btn btn-success" href="{{ route('admin.anggota.edit',$data->id) }}">Ubah</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection