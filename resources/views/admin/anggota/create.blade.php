@extends('layouts.admin')
@section('title', 'Anggota')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Anggota
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Anggota</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.anggota.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="col-md-12">
                            <div class="text-center">
                                <img src="{{ asset('itlabil/image/default/image.png') }}" id="category-img-tag" alt="" style="height: 200px;width:200px;object-fit:cover" class="img-thumbnail">
                                <p>PREVIEW</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Gambar</label>
                            <div class="col-sm-10">
                                <input type="file" name="photo" id="cat_image">
                                <i>*ukuran gambar 400x400, berupa .jpg .jpeg .png dan maksimal size 1MB. </i>
                                <small class="text-danger">{{ $errors->first('photo') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama</label>
                            <div class="col-sm-10">
                                <input type="text" name="name" class="form-control" placeholder="Nama Lengkap">
                                <small class="text-danger">{{ $errors->first('name') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tahun" class="col-sm-2 control-label">JK</label>

                            <div class="col-sm-4">
                                <select class="form-control" name="jk">
                                    <option value="Laki-Laki">Laki-Laki</option>
                                    <option value="Perempuan">Perempuan</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Email</label>
                            <div class="col-sm-10">
                                <input type="email" name="email" class="form-control" placeholder="Email">
                                <small class="text-danger">{{ $errors->first('email') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">No Induk</label>
                            <div class="col-sm-10">
                                <input type="number" name="no_induk" class="form-control" placeholder="No Induk">
                                <small class="text-danger">{{ $errors->first('no_induk') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">No Telpon</label>
                            <div class="col-sm-10">
                                <input type="text" name="no_telp" class="form-control" placeholder="No Telpon">
                                <small class="text-danger">{{ $errors->first('no_telp') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tahun" class="col-sm-2 control-label">Alamat</label>

                            <div class="col-sm-10">
                                <div class="box-body pad">
                                    <textarea class="textarea" name="alamat" placeholder="Content" id="isi" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px"></textarea>
                                </div>
                                <small class="text-danger">{{ $errors->first('isi') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="tahun" class="col-sm-2 control-label">Status</label>

                            <div class="col-sm-4">
                                <select class="form-control" name="status">
                                    <option value="Guru">Guru</option>
                                    <option value="Karyawan">Karyawan</option>
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/anggota')}}" class="btn btn-default  pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection