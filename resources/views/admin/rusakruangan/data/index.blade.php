@extends('layouts.admin')
@section('title', 'Rusak Ruangan')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Rusak Ruangan
        </h1>
    </section><br><br>

    <div class="row">
        
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Rusak Ruangan</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Ruang</th>
                                <th>Penginput</th>
                                <th>Barang</th>
                                <th>Jumlah Rusak</th>
                                <th>Tanggal Rusak</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->ruangan->ruang->ruang }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->ruangan->barang_detail->barang->barang }} {{ $item->ruangan->barang_detail->merk }}</td>
                                <td>{{ $item->jumlah }}</td>
                                <td>{{ $item->tanggal_rusak->format('d-m-Y')}}</td>
                                <td>
                                    @if($item->status==='Sudah diperbaiki')
                                        <span class="label label-success">{{ $item->status }}</span>
                                    @else
                                        <span class="label label-info">{{ $item->status }}</span>
                                    @endif
                                </td>
                                <td align="center" width="200px">
                                    @if($item->status==='Dalam perbaikan')
                                        <a class="btn btn-primary" href="{{ route('admin.rusakruangan.show',$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i></a>
                                        <a href="/admin/rusakruangan/status/{{ $item->id }}" class="btn btn-success"  onclick="return confirm('Apakah Anda Yakin ?')"><i class="fa fa-check"></i></a>
                                    @else
                                        <a class="btn btn-primary" href="{{ route('admin.rusakruangan.show',$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i></a>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection