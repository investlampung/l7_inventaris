@extends('layouts.admin')
@section('title', 'Barang Keluar')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Barang Keluar
        </h1>
    </section><br><br>

    <div class="row">
        
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Barang Keluar</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Peminjam</th>
                                <th>Barang</th>
                                <th>Jumlah</th>
                                <th>Tanggal Keluar</th>
                                <th>Keterangan</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->barang_detail->barang->barang }} {{ $item->barang_detail->merk }}</td>
                                <td>{{ $item->jumlah }}</td>
                                <td>{{ $item->tanggal_keluar->format('d-m-Y')}}</td>
                                <td>{{ $item->keterangan}}</td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection