@extends('layouts.admin')
@section('title', 'Keranjang Barang Keluar')
@section('content')
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>

<div class="container">

    <section class="content-header">
        <h1>
            Keranjang Barang Keluar
        </h1>
    </section><br><br>

    <div class="row">
        
        <div class="col-md-4">
            <button class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Keranjang Barang Keluar</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Peminjam</th>
                                <th>Barang</th>
                                <th>Jumlah</th>
                                <th>Tanggal Keluar</th>
                                <th>Keterangan</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->user->name }}</td>
                                <td>{{ $item->barang_detail->barang->barang }} {{ $item->barang_detail->merk }}</td>
                                <td>{{ $item->jumlah }}</td>
                                <td>{{ $item->tanggal_keluar->format('d-m-Y')}}</td>
                                <td>{{ $item->keterangan }}</td>
                                <td>
                                    @if($item->status==='Telah disetujui')
                                        <span class="label label-info">{{ $item->status }}</span>
                                    @else
                                        <span class="label label-warning">{{ $item->status }}</span>
                                    @endif
                                </td>
                                <td align="center" width="200px">
                                    @if($item->status==='Telah disetujui')
                                    <form action="{{ route('admin.keranjangkeluar.destroy',$item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger" alt="Hapus"><i class="fa fa-trash"></i></button>
                                    </form>
                                    @else
                                    <form action="{{ route('admin.keranjangkeluar.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.keranjangkeluar.edit',$item->id) }}" alt="Edit"><i class="fa fa-edit"></i></a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger" alt="Hapus"><i class="fa fa-trash"></i></button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="box-header with-border" align="center">
                    <a href="/inputkeluar" class="btn btn-primary">Masukan Semua Data</a>
                </div>
            </div>
        </div>

        <div id="tambah" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Masukan Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/admin/keranjangkeluar/tampil" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="video" style="form-control float:top" class="control-label">Scan QR Code</label><br>
                                <center><video name="video" id="preview" style="width: 400px"></video></center>
                            </div>
                            <div class="form-group">
                                <label for="">Pilih Peminjam</label>
                                <select name="anggota" class="myselect form-control" id="anggota"  style="width:100%">
                                    <option value="" selected disabled>Pilih Peminjam</option>
                                    @foreach ($users as $user)
                                        <option value="{{$user->id}}">{{$user->no_induk}} - {{$user->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Nama Peminjam</label>
                                <input type="hidden" name="no_induk" id="no_induk">
                                <input type="hidden" id="user_id" name="user_id" class="form-control"  required>
                                <input type="text" id="nama" readonly name="nama" class="form-control"  required>
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Keluar</label>
                                <input type="date" name="tanggal_keluar" class="form-control" required>
                            </div>
                            <div class="form-group">
                                <label for="">Keterangan</label>
                                <input type="text" id="keterangan" name="keterangan" class="form-control">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Lanjut</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
    scanner.addListener('scan', function (content) {
        var no_induk = content.substring(0, 6);
        $('#no_induk').val(no_induk);
        $.get("{{url ('get-data-anggota2') }}"+'/'+no_induk,function(data){
            $('#user_id').val(data.id);
            $('#nama').val(data.name);
        });
    });
    Instascan.Camera.getCameras().then(function (cameras) {
        if (cameras.length > 0) {
            scanner.start(cameras[0]);
        } else {
            console.error('No cameras found.');
        }
    }).catch(function (e) {
        console.error(e);
    });
</script>
@endsection

@section('script')
<script type="text/javascript">
    $("#anggota").change(function() {
         $.ajax({
             url: '/get-data-anggota/' + $(this).val(),
             type: 'get',
             data: {},
             success: function(data) {
             if (data.success == true) {
                 $('#nama').val(data.name);
                 $('#no_induk').val(data.no_induk);
                 $('#user_id').val(data.id);
             } else {
                 alert('Cannot find info');
             }
  
             },
             error: function(jqXHR, textStatus, errorThrown) {}
         });
     }); 
  </script>
@endsection