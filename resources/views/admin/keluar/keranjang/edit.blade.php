@extends('layouts.admin')
@section('title', 'Keranjang Barang Keluar')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Keranjang Barang Keluar
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Ubah Data Keranjang Barang Keluar</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.keranjangkeluar.update', $data->id) }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Nama Peminjam</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ $data->user->name }}" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Barang</label>
                            <div class="col-sm-10">
                                <input type="text" value="{{ $data->barang_detail->barang->barang }} {{ $data->barang_detail->merk }}" class="form-control" readonly>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="number" value="{{ $data->jumlah }}" name="jumlah" class="form-control" placeholder="{{ $data->jumlah_pinjam+$data->barang_detail->jumlah }}">
                                <small class="text-danger">{{ $errors->first('jumlah') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Tanggal Keluar</label>
                            <div class="col-sm-10">
                                <input type="date" name="tanggal_keluar" value="{{ $data->tanggal_keluar->format('Y-m-d') }}" class="form-control" placeholder="Tanggal Masuk">
                                <small class="text-danger">{{ $errors->first('tanggal_keluar') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Keterangan</label>
                            <div class="col-sm-10">
                                <input type="text" name="keterangan" value="{{ $data->keterangan }}" class="form-control" placeholder="Tanggal Masuk">
                                <small class="text-danger">{{ $errors->first('keterangan') }}</small>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/keranjangkeluar')}}" class="btn btn-default pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script type="text/javascript">

        function sum() {
            var txtFirstNumberValue = document.getElementById('txt1').value;
            var txtSecondNumberValue = document.getElementById('txt2').value;
            var result = parseFloat(txtFirstNumberValue) * parseFloat(txtSecondNumberValue);
            if (!isNaN(result)) {
                document.getElementById('txt3').value = result;
            }
        }

    </script>

@endpush
@endsection