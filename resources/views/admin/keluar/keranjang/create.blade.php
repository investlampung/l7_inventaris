@extends('layouts.admin')
@section('title', 'Keranjang Barang Keluar')
@section('content')
<script src="https://rawgit.com/schmich/instascan-builds/master/instascan.min.js"></script>
<div class="container">

    <section class="content-header">
        <h1>
            Keranjang Barang Keluar
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                {{-- <div class="box-header with-border">
                    <h3 class="box-title">Keranjang Peminjaman</h3>
                </div> --}}
                <div class="box-body" style="overflow-x:auto;">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table">
                                    <tr>
                                        <td>Nama Peminjam</td>
                                        <td>: {{ $users->name }}</td>
                                    </tr>
                                    <tr>
                                        <td>Tanggal Keluar</td>
                                        <td>: {{ tanggal_local(Carbon\Carbon::parse($tanggal_keluar)->format('Y-m-d')) }}</td>
                                    </tr>
                                    <tr>
                                        <td>Keterangan</td>
                                        <td>: {{ $keterangan }}</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <label for="video" style="form-control float:top" class="control-label">Scan QR Code</label><br>
                                            <center><video name="video" id="preview" style="width: 400px"></video></center>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <div class="table-responsive">
                                    <form class="form-horizontal" action="{{ route('admin.keranjangkeluar.store') }}" method="post">
                                        @csrf
                                        <br>
                                        <div class="input_fields_wrap">
                                            <button class="add_field_button btn btn-primary">Tambah Barang</button><br><br>
                                        </div>
                                        <input type="hidden" value="{{ $user_id }}" name="user_id" class="form-control" required>
                                        <input type="hidden" value="{{ $tanggal_keluar }}" name="tanggal_keluar" class="form-control" required>
                                        <input type="hidden" value="{{ $keterangan }}" name="keterangan" class="form-control" required>
                                        <br><br>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                </div>
            </div>
        </div>
      </div>
</div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function() {
            var max_fields      = 100; //maximum input boxes allowed
            var wrapper   		= $(".input_fields_wrap"); //Fields wrapper
            var add_button      = $(".add_field_button"); //Add button ID
        
            var x = 1; //initlal text box count
            $(add_button).click(function(e){ //on add input button click
                e.preventDefault();
                if(x < max_fields){ //max input box allowed
                    
                    let scanner = new Instascan.Scanner({ video: document.getElementById('preview') });
                    scanner.addListener('scan', function (content) {
                        var kode = content.substring(0, 10);
                        //$('#barang_id'+x).val(kode);
                        $.get("{{url ('get-data-barang2') }}"+'/'+kode,function(data){
                            $('#nama_barang'+x).val(data.nama);
                            $('#barang_id'+x).val(data.id);
                            var max = document.getElementById("number_id"+x);
                            max.setAttribute("max", data.max)
                            max.setAttribute("placeholder", "Jumlah Maksimal "+data.max)
                        });
                    });
                    Instascan.Camera.getCameras().then(function (cameras) {
                        if (cameras.length > 0) {
                            scanner.start(cameras[0]);
                        } else {
                            console.error('No cameras found.');
                        }
                    }).catch(function (e) {
                        console.error(e);
                    });

                    x++; //text box increment
                    $(wrapper).append('<div><select class="form-control" id="barang'+x+'"><option>Pilih Barang</option>@foreach ($barang as $data)@foreach ($data->barang_detail as $item)<option value="{{ $item->kode_barang }}">{{ $item->barang->barang}} {{ $item->merk}} ({{ $item->jumlah}})</option>@endforeach @endforeach</select><label for="">Barang</label><input type="hidden" id="barang_id'+x+'" name="barang_id[]"><input type="text" id="nama_barang'+x+'" name="nama_barang[]" class="form-control" required placeholder="Barang" required readonly><br><input id="number_id'+x+'" type="number" name="jumlah[]" class="form-control" required placeholder="Masukan Jumlah" required></td></tr></table><a href="#" class="remove_field">Remove</a><hr></div>');
                    
                    $("#barang"+x).change(function() {
                        $.get("{{url ('get-data-barang2') }}"+'/'+$(this).val(),function(data){
                            $('#nama_barang'+x).val(data.nama);
                            $('#barang_id'+x).val(data.id);
                            var max = document.getElementById("number_id"+x);
                            max.setAttribute("max", data.max)
                            max.setAttribute("placeholder", "Jumlah Maksimal "+data.max)
                        });
                    }); 
                }
            });

            $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
                e.preventDefault(); $(this).parent('div').remove(); x--;
            })
        });
    </script>
@endsection