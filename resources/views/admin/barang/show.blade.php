@extends('layouts.admin')
@section('title', 'Barang')
@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Barang
        </h1>
    </section><br><br>

    <div class="row">

        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Barang</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kode</th>
                                <th>Barang</th>
                                <th>Merk</th>
                                <th>Jumlah</th>
                                <th>Jumlah Rusak</th>
                                <th>Harga Satuan</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->kode_barang}}</td>
                                <td>{{ $item->barang->barang}}</td>
                                <td>{{ $item->merk}}</td>
                                <td>{{ $item->jumlah}}</td>
                                <td>{{ $item->jumlah_rusak}}</td>
                                <td>Rp. {{ number_format($item->harga_satuan, 0, ".", ".")}},-</td>
                                <td align="center" width="200px">
                                    <a class="btn btn-primary" href="{{ route('admin.barangdetail.show',$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i></a>
                                    {{-- <form action="{{ route('admin.barangmasuk.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.barangmasuk.edit',$item->id) }}" alt="Edit"><i class="fa fa-edit"></i></a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger" alt="Hapus"><i class="fa fa-trash"></i></button>
                                    </form> --}}
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection