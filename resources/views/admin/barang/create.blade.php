@extends('layouts.admin')
@section('title', 'Barang')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Barang
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Barang</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.barang.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label for="tahun" class="col-sm-2 control-label">Kategori</label>

                            <div class="col-sm-10">
                                <select class="form-control" name="kategori_id">
                                    @foreach ($kategori as $item)
                                        <option value="{{ $item->id }}">{{ $item->kategori}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Barang</label>
                            <div class="col-sm-10">
                                <input type="text" name="barang" class="form-control" placeholder="Nama Barang">
                                <small class="text-danger">{{ $errors->first('barang') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Satuan</label>
                            <div class="col-sm-10">
                                <input type="text" name="satuan" class="form-control" placeholder="Satuan">
                                <small class="text-danger">{{ $errors->first('satuan') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jumlah</label>
                            <div class="col-sm-10">
                                <input type="number" name="jumlah" value=0 class="form-control" placeholder="Jumlah">
                                <small class="text-danger">{{ $errors->first('jumlah') }}</small>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Jumlah Rusak</label>
                            <div class="col-sm-10">
                                <input type="number" name="jumlah_rusak" value=0 class="form-control" placeholder="Jumlah Rusak">
                                <small class="text-danger">{{ $errors->first('jumlah_rusak') }}</small>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/barang')}}" class="btn btn-default  pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection