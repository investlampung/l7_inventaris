@extends('layouts.admin')
@section('title', 'Barang')
@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Barang
        </h1>
    </section><br><br>

    <div class="row">
        
        <div class="col-md-4">
            <a href="{{ route('admin.barang.create') }}" class="btn btn-primary">+ Barang</a><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Barang</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Barang</th>
                                <th>Kategori</th>
                                <th>Satuan</th>
                                <th>Jumlah</th>
                                <th>Jumlah Rusak</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->barang}}</td>
                                <td>{{ $item->kategori->kategori}}</td>
                                <td>{{ $item->satuan}}</td>
                                <td>{{ $item->barang_detail->sum('jumlah')}}</td>
                                <td>{{ $item->barang_detail->sum('jumlah_rusak')}}</td>
                                <td align="center" width="200px">
                                    <form action="{{ route('admin.barang.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-primary" href="{{ route('admin.barang.show',$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i></a>
                                        <a class="btn btn-success" href="{{ route('admin.barang.edit',$item->id) }}" alt="Edit"><i class="fa fa-edit"></i></a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger" alt="Hapus"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection