@extends('layouts.admin')
@section('title', 'Barang Ruangan')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Barang Ruangan
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Barang Ruangan</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Ruangan</th>
                                <th>Jumlah Barang</th>
                                <th>Jumlah Barang Rusak</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->ruang }}</td>
                                <td>{{ $item->ruangan->sum('jumlah') }}</td>
                                <td>{{ $item->ruangan->sum('jumlah_rusak') }}</td>
                                <td align="center" width="200px">
                                    <button class="btn btn-primary" data-toggle="modal" data-target="#lihat{{ $item->id }}"><i class="fa fa-eye"></i></button>
                                    <div id="lihat{{ $item->id }}" class="modal fade" tabindex="-1" role="dialog">
                                        <div class="modal-dialog" role="document">
                                            <!-- Modal content-->
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h4 class="modal-title">Barang Ruang {{ $item->ruang }}</h4>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body">
                                                    <table class="table table-bordered table-striped">
                                                        <thead>
                                                            <tr>
                                                                <th>Nama Barang</th>
                                                                <th>Jumlah Barang</th>
                                                                <th>Jumlah Barang Rusak</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            @foreach($item->ruangan as $ruangan)
                                                                <tr>
                                                                    <td>{{ $ruangan->barang_detail->barang->barang }} {{ $ruangan->barang_detail->merk }}</td>
                                                                    <td>{{ $ruangan->jumlah }}</td>
                                                                    <td>{{ $ruangan->jumlah_rusak }}</td>
                                                                </tr>
                                                            @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-body">
                                                    <a class="btn btn-primary" href="{{ url('admin/ruangan/tampil/'.$item->id) }}" alt="Lihat"><i class="fa fa-eye"></i> Lihat Detail</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection