@extends('layouts.admin')
@section('title', 'Detail Barang Ruang')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Detail Barang Ruang {{ $ruang->ruang }}
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Barang Ruang {{ $ruang->ruang }}</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <div class="col-md-12">
                        <table id="example1" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Barang</th>
                                    <th>Jumlah</th>
                                    <th>Tanggal Masuk</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($data as $item)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $item->barang_detail->barang->barang }} {{ $item->barang_detail->merk }}</td>
                                    <td>{{ $item->jumlah }}</td>
                                    <td>{{ $item->tanggal_masuk->format('d-m-Y')}}</td>
                                </tr>
                                @endforeach
    
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <a href="{{url('admin/ruangan')}}" class="btn btn-default">Kembali</a>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection