@extends('layouts.admin')
@section('title', 'Keranjang Barang Ruangan')
@section('content')

<div class="container">

    <section class="content-header">
        <h1>
            Keranjang Barang Ruangan
        </h1>
    </section><br><br>

    <div class="row">
        
        <div class="col-md-4">
            <button class="btn btn-success" data-toggle="modal" data-target="#tambah">Tambah Data</button><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Keranjang Barang Ruangan</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Nama Ruangan</th>
                                <th>Barang</th>
                                <th>Jumlah</th>
                                <th>Tanggal Masuk</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->ruang->ruang }}</td>
                                <td>{{ $item->barang_detail->barang->barang }} {{ $item->barang_detail->merk }}</td>
                                <td>{{ $item->jumlah }}</td>
                                <td>{{ $item->tanggal_masuk->format('d-m-Y')}}</td>
                                <td>
                                    @if($item->status==='Telah disetujui')
                                        <span class="label label-info">{{ $item->status }}</span>
                                    @else
                                        <span class="label label-warning">{{ $item->status }}</span>
                                    @endif
                                </td>
                                <td align="center" width="200px">
                                    @if($item->status==='Telah disetujui')
                                    <form action="{{ route('admin.keranjangruangan.destroy',$item->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger" alt="Hapus"><i class="fa fa-trash"></i></button>
                                    </form>
                                    @else
                                    <form action="{{ route('admin.keranjangruangan.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.keranjangruangan.edit',$item->id) }}" alt="Edit"><i class="fa fa-edit"></i></a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger" alt="Hapus"><i class="fa fa-trash"></i></button>
                                    </form>
                                    @endif
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
                <div class="box-header with-border" align="center">
                    <a href="/inputruangan" class="btn btn-primary">Masukan Semua Data</a>
                </div>
            </div>
        </div>

        <div id="tambah" class="modal fade" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Masukan Data</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <form action="/admin/keranjangruangan/tampil" method="post">
                            @csrf
                            <div class="form-group">
                                <label for="">Pilih Ruang</label>
                                <select name="ruang_id" class="form-control" id="ruang"  style="width:100%">
                                    <option value="" selected disabled>Pilih Ruang</option>
                                    @foreach ($ruang as $ruang)
                                        <option value="{{$ruang->id}}">{{$ruang->ruang}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Tanggal Masuk</label>
                                <input type="date" name="tanggal_masuk" class="form-control" required>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Lanjut</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection