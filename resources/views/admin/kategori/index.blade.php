@extends('layouts.admin')
@section('title', 'Kategori')
@section('content')


<div class="container">

    <section class="content-header">
        <h1>
            Kategori
        </h1>
    </section><br><br>

    <div class="row">
        
        <div class="col-md-4">
            <a href="{{ route('admin.kategori.create') }}" class="btn btn-primary">+ Kategori</a><br><br>
        </div>
        <div class="col-md-12">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Data Kategori</h3>
                </div>
                <div class="box-body" style="overflow-x:auto;">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Kategori</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $item)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $item->kategori}}</td>
                                <td align="center" width="200px">
                                    <form action="{{ route('admin.kategori.destroy',$item->id) }}" method="POST">
                                        <a class="btn btn-success" href="{{ route('admin.kategori.edit',$item->id) }}" alt="Edit"><i class="fa fa-edit"></i></a>

                                        @csrf
                                        @method('DELETE')

                                        <button type="submit" class="btn btn-danger" alt="Hapus"><i class="fa fa-trash"></i></button>
                                    </form>
                                </td>
                            </tr>
                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

</div>

@endsection