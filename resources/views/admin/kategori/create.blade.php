@extends('layouts.admin')
@section('title', 'Kategori')
@section('content')
<div class="container">

    <section class="content-header">
        <h1>
            Kategori
        </h1>
    </section><br><br>

    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">Tambah Kategori</h3>
                </div>

                <div class="box-body" style="overflow-x:auto;">
                    <form action="{{ route('admin.kategori.store') }}" class="form-horizontal" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Kategori</label>
                            <div class="col-sm-10">
                                <input type="text" name="kategori" class="form-control" placeholder="Kategori">
                                <small class="text-danger">{{ $errors->first('kategori') }}</small>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <button type="submit" class="btn btn-primary pull-right">Simpan</button>
                            <a href="{{url('admin/kategori')}}" class="btn btn-default  pull-right">Kembali</a>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection