<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AnggotaRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                return [
                    'photo' => 'image|mimes:jpeg,jpg,png|max:1000',
                    'email'  => 'required|unique:users' ,
                    'name'     => 'required',
                    'no_induk'  => 'required|numeric',
                ];
            }

            case 'PUT':
            case 'PATCH': {
                return [
                    'photo' => 'image|mimes:jpeg,jpg,png|max:1000',
                    'email'  => 'required' ,
                    'name'     => 'required',
                    'no_induk'  => 'required|numeric',
                ];
            }

            default:
                break;
        }
        
    }
    public function messages()
    {
        return [
            'photo.image'           => 'Hanya photo/gambar',
            'photo.max'             => 'Size maksimal 2MB',
            'photo.mimes'           => 'Gambar / Image .jpg .jpeg .png',
            'email.required'        => 'Tidak boleh kosong',
            'email.unique'          => 'Email sudah digunakan',
            'name.required'         => 'Tidak boleh kosong',
            'no_induk.required'     => 'Tidak boleh kosong',
            'no_induk.numeric'      => 'Hanya angka.'
        ];
    }
}
