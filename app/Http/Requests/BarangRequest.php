<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BarangRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'barang'  => 'required|unique:barangs',
                        'satuan'  => 'required',
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'barang'  => 'required',
                        'satuan'  => 'required',
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [
            'barang.required'        => 'Tidak boleh kosong',
            'barang.unique'          => 'Barang sudah ada',
            'satuan.required'        => 'Tidak boleh kosong',
        ];
    }
}
