<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RuangRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                return [
                    'ruang'  => 'required|unique:ruangs' ,
                ];
            }

            case 'PUT':
            case 'PATCH': {
                return [
                    'ruang'  => 'required' ,
                ];
            }

            default:
                break;
        }
        
    }
    public function messages()
    {
        return [
            'ruang.required'        => 'Tidak boleh kosong',
            'ruang.unique'          => 'Nama ruang sudah digunakan'
        ];
    }
}
