<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KategoriRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'kategori'     => 'required',
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'kategori'     => 'required',
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [
            'kategori.required'         => 'Tidak boleh kosong'
        ];
    }
}
