<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BarangMasukRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }


    public function rules()
    {
        switch ($this->method()) {
            case 'POST': {
                    return [
                        'jumlah'  => 'required',
                        'harga_satuan'  => 'required',
                        'sumber_dana'  => 'required',
                        'merk'  => 'required',
                    ];
                }

            case 'PUT':
            case 'PATCH': {
                    return [
                        'jumlah'  => 'required',
                        'harga_satuan'  => 'required',
                        'sumber_dana'  => 'required',
                        'merk'  => 'required',
                    ];
                }

            default:
                break;
        }
    }
    public function messages()
    {
        return [
            'jumlah.required'        => 'Tidak boleh kosong',
            'harga_satuan.required'        => 'Tidak boleh kosong',
            'sumber_dana.required'        => 'Tidak boleh kosong',
            'merk.required'        => 'Tidak boleh kosong',
        ];
    }
}
