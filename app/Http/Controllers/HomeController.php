<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->role == 'admin') {
            return redirect()->route('admin.beranda.index');
        } else if (auth()->user()->role == 'sarpras') {
            return redirect()->route('sarpras.beranda.index');
        } else {
            return redirect()->route('anggota.beranda.index');
        }
    }
}
