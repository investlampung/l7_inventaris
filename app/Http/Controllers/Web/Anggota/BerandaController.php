<?php

namespace App\Http\Controllers\Web\Anggota;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BerandaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:anggota');
    }

    public function index()
    {

        return view('anggota.beranda.index');
    }
}
