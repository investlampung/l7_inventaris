<?php

namespace App\Http\Controllers\Web\Sarpras;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BerandaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:sarpras');
    }

    public function index()
    {

        return view('sarpras.beranda.index');
    }
}
