<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BarangRequest;
use App\Model\Barang;
use App\Model\BarangDetail;
use App\Model\Kategori;

class BarangController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Barang::orderBy('barang', 'ASC')->get()->all();
        return view('admin.barang.index', compact('data', 'no'));
    }

    public function create()
    {
        $kategori = Kategori::orderBy('kategori', 'ASC')->get()->all();
        return view('admin.barang.create', compact('kategori'));
    }

    public function show($id)
    {
        $no = 1;
        $data = BarangDetail::where('barang_id', $id)->orderBy('tanggal_masuk', 'DESC')->get()->all();
        return view('admin.barang.show', compact('data', 'no'));
    }

    public function store(BarangRequest $request)
    {

        // SIMPAN DATA SISWA META
        $save = $request->all();

        Barang::create($save);

        $notification = array(
            'message' => 'Data Barang berhasil ditambah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.barang.index')->with($notification);
    }

    public function edit($id)
    {
        $kategori = Kategori::orderBy('kategori', 'ASC')->get()->all();
        $data = Barang::findOrFail($id);
        return view('admin.barang.edit', compact('data', 'kategori'));
    }

    public function update(BarangRequest $request, $id)
    {
        $data = Barang::findOrFail($id);

        $dataupdate = $request->all();

        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data Barang berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.barang.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Barang::findOrFail($id);

        $notification = array(
            'message' => 'Data barang berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.barang.index')->with($notification);
    }
}
