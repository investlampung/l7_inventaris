<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\BarangDetail;
use Illuminate\Http\Request;

class BarangDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function show($id)
    {
        $data = BarangDetail::findOrFail($id);
        return view('admin.barangdetail.show', compact('data'));
    }
}
