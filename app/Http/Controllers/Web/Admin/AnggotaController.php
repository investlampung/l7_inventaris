<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\AnggotaRequest;
use App\User;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class AnggotaController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = User::where('role', 'anggota')->where('hapus', 0)->orderBy('name', 'ASC')->get()->all();
        return view('admin.anggota.index', compact('data', 'no'));
    }

    public function create()
    {
        return view('admin.anggota.create');
    }

    public function store(AnggotaRequest $request)
    {

        // SIMPAN DATA SISWA META
        $save = $request->all();
        $save['password'] = bcrypt('1sampai9');
        $save['role'] = 'anggota';
        $save['hapus'] = 0;

        if ($request->hasFile('photo')) {
            $save['photo'] = $this->savePhoto($request->file('photo'));
        } else {
            $save['photo'] = '-';
        }

        User::create($save);

        $notification = array(
            'message' => 'Data anggota berhasil ditambah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.anggota.index')->with($notification);
    }

    public function show($id)
    {
        $data = User::findOrFail($id);
        return view('admin.anggota.show', compact('data'));
    }

    public function edit($id)
    {
        $data = User::findOrFail($id);
        return view('admin.anggota.edit', compact('data'));
    }

    public function update(AnggotaRequest $request, $id)
    {
        $data = User::findOrFail($id);

        $dataupdate = $request->all();

        if ($request->hasFile('photo')) {
            $this->deletePhoto($data->photo);
            $dataupdate['photo'] = $this->savePhoto($request->file('photo'));
        }

        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data Anggota berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.anggota.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = User::findOrFail($id);

        $this->deletePhoto($data->photo);

        $notification = array(
            'message' => 'Data anggota berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.anggota.index')->with($notification);
    }

    public function hapus($id)
    {
        $data = User::findOrFail($id);
        $dataupdate['hapus'] = 1;
        $notification = array(
            'message' => 'Data anggota berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->update($dataupdate);

        return redirect()->route('admin.anggota.index')->with($notification);
    }



    protected function savePhoto(UploadedFile $photo)
    {
        $fileName = Str::random(40) . '.' . $photo->guessClientExtension();
        $destinationPath = 'itlabil/image/pp';
        $photo->move($destinationPath, $fileName);
        return $fileName;
    }

    public function deletePhoto($filename)
    {
        $path = 'itlabil/image/pp/' . $filename;
        return File::delete($path);
    }
}
