<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\BarangDetail;
use App\Model\Ruangan;
use Illuminate\Http\Request;

class Barang extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function getDataBarang2($id)
    {
        $barang = BarangDetail::where('kode_barang', $id)->get()->first();
        $nama_barang = $barang->kode_barang . ' - ' . $barang->barang->barang . ' ' . $barang->merk;
        return response()->json(['nama' => $nama_barang, 'max' => $barang->jumlah, 'id' => $barang->id]);
    }

    public function getDataBarang3($id)
    {
        $data = Ruangan::where('id',$id)->first();
        $nama_barang = $data->barang_detail->kode_barang . ' - ' . $data->barang_detail->barang->barang . ' ' . $data->barang_detail->merk;
        return response()->json(['nama' => $nama_barang, 'max' => $data->jumlah, 'id' => $data->id]);
    }
}
