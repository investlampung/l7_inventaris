<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AkunController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = User::where('hapus', 0)->orderBy('status', 'ASC')->get()->all();
        return view('admin.akun.index', compact('data', 'no'));
    }

    public function edit($id)
    {
        $data = User::findOrFail($id);
        return view('admin.akun.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = User::findOrFail($id);

        if ($request->password=='') {
            $dataupdate['email'] = $request->email;
        } else {
            $dataupdate['email'] = $request->email;
            $dataupdate['password'] = bcrypt($request->password);
        }
        
        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data Akun berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.akun.index')->with($notification);
    }
}
