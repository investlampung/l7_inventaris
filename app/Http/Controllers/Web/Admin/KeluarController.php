<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\Keluar;
use Illuminate\Http\Request;

class KeluarController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Keluar::orderBy('id', 'DESC')->get()->all();
        return view('admin.keluar.data.index', compact('data', 'no'));
    }
}
