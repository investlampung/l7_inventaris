<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\Ruang;
use App\Model\Ruangan;
use App\Model\RuanganDetail;
use Illuminate\Http\Request;

class RuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Ruang::orderBy('ruang', 'ASC')->get()->all();
        return view('admin.ruangan.data.index', compact('data', 'no'));
    }

    public function tampil($ruang)
    {
        $no = 1;
        $data = RuanganDetail::where('ruang_id', $ruang)->orderBy('tanggal_masuk','DESC')->get();
        $ruang = Ruang::where('id',$ruang)->first();
        return view('admin.ruangan.data.show', compact('data', 'no','ruang'));
    }
}
