<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\Barang;
use App\Model\BarangDetail;
use App\Model\Ruang;
use App\Model\Ruangan;
use App\Model\RuanganDetail;
use App\Model\RuanganKeranjang;
use Illuminate\Http\Request;

class RuanganKeranjangController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $ruang = Ruang::orderBy('ruang', 'ASC')->get()->all();
        $data = RuanganKeranjang::orderBy('id', 'DESC')->get()->all();
        return view('admin.ruangan.keranjang.index', compact('data', 'no', 'ruang'));
    }

    public function tampil(Request $request)
    {
        $tanggal_masuk = $request->tanggal_masuk;
        $ruang_id = $request->ruang_id;

        $barang = Barang::orderBy('barang', 'ASC')->get()->all();
        $ruang = Ruang::where('id', $ruang_id)->get()->first();

        return view('admin.ruangan.keranjang.create', compact('barang', 'ruang', 'tanggal_masuk'));
    }

    public function store(Request $request)
    {

        for ($a = 0; $a < count($request->barang_id); $a++) {
            $pk = BarangDetail::where('id', $request->barang_id[$a])->get()->first();

            if ($pk->jumlah >= $request->jumlah[$a]) {
                RuanganKeranjang::create([
                    'ruang_id' =>  $request->ruang_id,
                    'barang_detail_id' =>  $request->barang_id[$a],
                    'jumlah' => $request->jumlah[$a],
                    'tanggal_masuk' => $request->tanggal_masuk,
                    'status' => 'Menunggu Persetujuan',
                ]);

                $bd = BarangDetail::where('id', $request->barang_id[$a])->get()->first();
                $bdupdate['jumlah'] = $bd->jumlah - $request->jumlah[$a];
                $bd->update($bdupdate);

                $notification = array(
                    'message' => 'Data berhasil ditambah ke keranjang.',
                    'alert-type' => 'success'
                );
            } else {
                $notification = array(
                    'message' => 'Stok barang tidak memadai.',
                    'alert-type' => 'warning'
                );
            }
        }

        return redirect()->route('admin.keranjangruangan.index')->with($notification);
    }

    public function edit($id)
    {
        $data = RuanganKeranjang::findOrFail($id);
        return view('admin.ruangan.keranjang.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {

        $data = RuanganKeranjang::findOrFail($id);

        $dataupdate = $request->all();

        if ($data->jumlah == $request->jumlah) {
            $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
            $bdupdate['jumlah'] = $bd->jumlah;
            $bd->update($bdupdate);
        } else if ($data->jumlah > $request->jumlah) {
            $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
            $kurang = $data->jumlah - $request->jumlah;
            $bdupdate['jumlah'] = $bd->jumlah + $kurang;
            $bd->update($bdupdate);
        } else {
            $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
            $lebih = $request->jumlah - $data->jumlah;
            $bdupdate['jumlah'] = $bd->jumlah - $lebih;
            $bd->update($bdupdate);
        }

        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data keranjang ruangan berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.keranjangruangan.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = RuanganKeranjang::findOrFail($id);

        $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
        $bdupdate['jumlah'] = $bd->jumlah + $data->jumlah;
        $bd->update($bdupdate);

        $notification = array(
            'message' => 'Data dikeranjang berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.keranjangruangan.index')->with($notification);
    }

    public function input()
    {

        $select = RuanganKeranjang::where('status', 'Telah disetujui')->get()->all();

        foreach ($select as $s) {
            $ruangan = Ruangan::where('ruang_id',$s->ruang_id)->where('barang_detail_id',$s->barang_detail_id)->count();
            if ($ruangan>0) {
                $dtruangan = Ruangan::where('ruang_id',$s->ruang_id)->where('barang_detail_id',$s->barang_detail_id)->get()->first();
                $dtupdate['jumlah'] = $dtruangan->jumlah + $s->jumlah;
                $dtruangan->update($dtupdate);
            } else {
                Ruangan::create([
                    'ruang_id' =>  $s->ruang_id,
                    'barang_detail_id' =>  $s->barang_detail_id,
                    'jumlah' =>  $s->jumlah,
                    'jumlah_rusak' => 0
                ]);
            }
            
            RuanganDetail::create([
                'ruang_id' =>  $s->ruang_id,
                'barang_detail_id' =>  $s->barang_detail_id,
                'jumlah' =>  $s->jumlah,
                'tanggal_masuk' =>  $s->tanggal_masuk
            ]);
        }

        foreach ($select as $s) {
            $data = RuanganKeranjang::findOrFail($s->id);
            $data->delete();
        }

        $notification = array(
            'message' => 'Data dikeranjang berhasil dimasukkan.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);;
    }
}
