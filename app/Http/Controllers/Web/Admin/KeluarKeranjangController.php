<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\Barang;
use App\Model\BarangDetail;
use App\Model\Keluar;
use App\Model\KeluarKeranjang;
use App\User;
use Illuminate\Http\Request;

class KeluarKeranjangController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $users = User::where('role', 'anggota')->where('hapus', 0)->orderBy('name', 'ASC')->get()->all();
        $data = KeluarKeranjang::orderBy('id', 'DESC')->get()->all();
        return view('admin.keluar.keranjang.index', compact('data', 'no', 'users'));
    }

    public function tampil(Request $request)
    {
        $tanggal_keluar = $request->tanggal_keluar;
        $keterangan = $request->keterangan;
        $user_id = $request->user_id;

        $barang = Barang::orderBy('barang', 'ASC')->get()->all();
        $users = User::where('id', $user_id)->get()->first();

        return view('admin.keluar.keranjang.create', compact('barang', 'users', 'user_id', 'tanggal_keluar', 'keterangan'));
    }

    public function store(Request $request)
    {

        for ($a = 0; $a < count($request->barang_id); $a++) {
            $pk = BarangDetail::where('id', $request->barang_id[$a])->get()->first();

            if ($pk->jumlah >= $request->jumlah[$a]) {
                KeluarKeranjang::create([
                    'user_id' =>  $request->user_id,
                    'barang_detail_id' =>  $request->barang_id[$a],
                    'jumlah' => $request->jumlah[$a],
                    'tanggal_keluar' => $request->tanggal_keluar,
                    'keterangan' => $request->keterangan,
                    'status' => 'Menunggu Persetujuan',
                ]);

                $bd = BarangDetail::where('id', $request->barang_id[$a])->get()->first();
                $bdupdate['jumlah'] = $bd->jumlah - $request->jumlah[$a];
                $bd->update($bdupdate);

                $notification = array(
                    'message' => 'Data barang keluar berhasil ditambah ke keranjang.',
                    'alert-type' => 'success'
                );
            } else {
                $notification = array(
                    'message' => 'Stok barang tidak memadai.',
                    'alert-type' => 'warning'
                );
            }
        }

        return redirect()->route('admin.keranjangkeluar.index')->with($notification);
    }

    public function edit($id)
    {
        $data = KeluarKeranjang::findOrFail($id);
        return view('admin.keluar.keranjang.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {

        $data = KeluarKeranjang::findOrFail($id);

        $dataupdate = $request->all();

        if ($data->jumlah == $request->jumlah) {
            $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
            $bdupdate['jumlah'] = $bd->jumlah;
            $bd->update($bdupdate);
        } else if ($data->jumlah > $request->jumlah) {
            $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
            $kurang = $data->jumlah - $request->jumlah;
            $bdupdate['jumlah'] = $bd->jumlah + $kurang;
            $bd->update($bdupdate);
        } else {
            $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
            $lebih = $request->jumlah - $data->jumlah;
            $bdupdate['jumlah'] = $bd->jumlah - $lebih;
            $bd->update($bdupdate);
        }

        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data barang keluar berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.keranjangkeluar.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = KeluarKeranjang::findOrFail($id);

        $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
        $bdupdate['jumlah'] = $bd->jumlah + $data->jumlah;
        $bd->update($bdupdate);

        $notification = array(
            'message' => 'Data barang keluar dikeranjang berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.keranjangkeluar.index')->with($notification);
    }

    public function input()
    {

        $select = KeluarKeranjang::where('status', 'Telah disetujui')->get()->all();

        foreach ($select as $s) {
            Keluar::create([
                'user_id' =>  $s->user_id,
                'barang_detail_id' =>  $s->barang_detail_id,
                'jumlah' =>  $s->jumlah,
                'tanggal_keluar' =>  $s->tanggal_keluar,
                'keterangan' =>  $s->keterangan,
            ]);
        }

        foreach ($select as $s) {
            $data = KeluarKeranjang::findOrFail($s->id);
            $data->delete();
        }

        $notification = array(
            'message' => 'Data barang keluar dikeranjang berhasil dimasukkan.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);;
    }
}
