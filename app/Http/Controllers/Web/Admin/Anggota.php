<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use DB;

class Anggota extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function getDataAnggota($id)
    {
        // $anggota = DB::table('anggotas')->where('no_induk', $id)->get()->first();
        // return response()->json($anggota);
        $name = DB::table('users')->where('id', $id)->pluck('name');
        $no_induk = DB::table('users')->where('id', $id)->pluck('no_induk');
        return response()->json(['success' => true, 'name' => $name, 'no_induk' => $no_induk, 'id' => $id]);
    }

    public function getDataAnggota2($id)
    {
        $anggota = User::where('no_induk', $id)->get()->first();
        return response()->json($anggota);
    }

    public function dataAjax(Request $request)
    {
        $data = [];
        if ($request->has('q')) {
            $search = $request->q;
            $data = DB::table("users")
                ->select("id", "name")
                ->where('name', 'LIKE', "%$search%")
                ->get();
        }


        return response()->json($data);
    }
}
