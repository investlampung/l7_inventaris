<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\BarangDetail;
use App\Model\Ruang;
use App\Model\Ruangan;
use App\Model\RusakRuangan;
use App\Model\RusakRuanganKeranjang;
use Illuminate\Http\Request;
use DB;

class RusakRuanganKeranjangController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $ruang = Ruang::orderBy('ruang', 'ASC')->get()->all();
        $data = RusakRuanganKeranjang::orderBy('id', 'DESC')->get()->all();
        return view('admin.rusakruangan.keranjang.index', compact('data', 'no', 'ruang'));
    }

    public function tampil(Request $request)
    {
        $tanggal_rusak = $request->tanggal_rusak;
        $ruang_id = $request->ruang_id;

        $barang = Ruangan::where('ruang_id', $ruang_id)->get();
        $ruang = Ruang::where('id', $ruang_id)->first();

        return view('admin.rusakruangan.keranjang.create', compact('barang', 'ruang', 'tanggal_rusak'));
    }

    public function store(Request $request)
    {
        // dd($request->jumlah[0]);
        for ($a = 0; $a < count($request->nama_barang); $a++) {
            $ruang = Ruangan::findOrFail($request->ruangan_id[$a]);
            if ($ruang->jumlah >= $request->jumlah[$a]) {
                RusakRuanganKeranjang::create([
                    'user_id' =>  $request->user_id,
                    'ruangan_id' =>  $request->ruangan_id[$a],
                    'jumlah' => $request->jumlah[$a],
                    'tanggal_rusak' => $request->tanggal_rusak,
                    'status' => 'Rusak',
                ]);

                $ruang = Ruangan::findOrFail($request->ruangan_id[$a]);
                $dataup['jumlah'] =  $ruang->jumlah - $request->jumlah[$a];
                $dataup['jumlah_rusak'] =  $ruang->jumlah_rusak + $request->jumlah[$a];
                $ruang->update($dataup);

                $notification = array(
                    'message' => 'Data berhasil ditambah ke keranjang.',
                    'alert-type' => 'success'
                );
            } else {
                $notification = array(
                    'message' => 'Stok barang tidak memadai.',
                    'alert-type' => 'warning'
                );
            }
        }

        return redirect()->route('admin.keranjangrusakruangan.index')->with($notification);
    }

    public function edit($id)
    {
        $data = RusakRuanganKeranjang::findOrFail($id);
        return view('admin.rusakruangan.keranjang.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {

        $data = RusakRuanganKeranjang::findOrFail($id);

        $dataupdate = $request->all();

        if ($data->jumlah == $request->jumlah) {
            $bd = Ruangan::where('id', $data->ruangan_id)->get()->first();
            $bdupdate['jumlah'] = $bd->jumlah;
            $bd->update($bdupdate);
        } else if ($data->jumlah > $request->jumlah) {
            $bd = Ruangan::where('id', $data->ruangan_id)->get()->first();
            $kurang = $data->jumlah - $request->jumlah;
            $bdupdate['jumlah'] = $bd->jumlah + $kurang;
            $bdupdate['jumlah_rusak'] = $request->jumlah;
            $bd->update($bdupdate);
        } else {
            $bd = Ruangan::where('id', $data->ruangan_id)->get()->first();
            $lebih = $request->jumlah - $data->jumlah;
            $bdupdate['jumlah'] = $bd->jumlah - $lebih;
            $bdupdate['jumlah_rusak'] = $request->jumlah;
            $bd->update($bdupdate);
        }

        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data keranjang ruangan berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.keranjangrusakruangan.index')->with($notification);
    }

    public function input()
    {

        $select = RusakRuanganKeranjang::where('status', 'Dalam perbaikan')->get()->all();

        foreach ($select as $s) {
            RusakRuangan::create([
                'user_id' =>  $s->user_id,
                'ruangan_id' =>  $s->ruangan_id,
                'jumlah' =>  $s->jumlah,
                'tanggal_rusak' =>  $s->tanggal_rusak,
                'status' => $s->status
            ]);
        }

        foreach ($select as $s) {
            $data = RusakRuanganKeranjang::findOrFail($s->id);
            $data->delete();
        }

        $notification = array(
            'message' => 'Data dikeranjang berhasil dimasukkan.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);;
    }
}
