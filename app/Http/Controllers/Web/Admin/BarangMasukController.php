<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BarangMasukRequest;
use App\Model\Barang;
use App\Model\BarangDetail;
use App\Model\BarangMasuk;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BarangMasukController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = BarangMasuk::orderBy('tanggal_masuk', 'DESC')->get()->all();
        return view('admin.barangmasuk.data.index', compact('data', 'no'));
    }

    public function show($id)
    {
        $data = BarangMasuk::findOrFail($id);
        return view('admin.barangmasuk.data.show', compact('data'));
    }

    public function edit($id)
    {
        $barang = Barang::orderBy('barang', 'ASC')->get()->all();
        $data = BarangMasuk::findOrFail($id);
        return view('admin.barangmasuk.data.edit', compact('data', 'barang'));
    }

    public function update(BarangMasukRequest $request, $id)
    {
        
        $data = BarangMasuk::findOrFail($id);

        $dataupdate = $request->all();

        if ($data->jumlah == $request->jumlah) {
            $bd = BarangDetail::where('kode_barang', $data->kode_barang)->get()->first();
            $bdupdate = $request->all();
            $bdupdate['jumlah'] = $bd->jumlah;
            $bd->update($bdupdate);
        } else if ($data->jumlah > $request->jumlah) {
            $bd = BarangDetail::where('kode_barang', $data->kode_barang)->get()->first();
            $bdupdate = $request->all();
            $kurang = $data->jumlah - $request->jumlah;
            $bdupdate['jumlah'] = $bd->jumlah - $kurang;
            $bd->update($bdupdate);
        } else {
            $bd = BarangDetail::where('kode_barang', $data->kode_barang)->get()->first();
            $bdupdate = $request->all();
            $lebih = $request->jumlah - $data->jumlah;
            $bdupdate['jumlah'] = $bd->jumlah + $lebih;
            $bd->update($bdupdate);
        }

        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data barang masuk berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.barangmasuk.index')->with($notification);
    }
}
