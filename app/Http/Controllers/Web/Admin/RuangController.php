<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\RuangRequest;
use App\Model\Ruang;
use App\User;
use Illuminate\Http\Request;

class RuangController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Ruang::orderBy('ruang', 'ASC')->get()->all();
        return view('admin.ruang.index', compact('data', 'no'));
    }

    public function create()
    {
        $anggota = User::where('role', 'anggota')->where('hapus', 0)->get()->all();
        return view('admin.ruang.create', compact('anggota'));
    }

    public function store(RuangRequest $request)
    {

        // SIMPAN DATA SISWA META
        $save = $request->all();

        Ruang::create($save);

        $notification = array(
            'message' => 'Data ruang berhasil ditambah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.ruang.index')->with($notification);
    }

    public function edit($id)
    {
        $anggota = User::where('role', 'anggota')->where('hapus', 0)->get()->all();
        $data = Ruang::findOrFail($id);
        return view('admin.ruang.edit', compact('data', 'anggota'));
    }

    public function update(RuangRequest $request, $id)
    {
        $data = Ruang::findOrFail($id);

        $dataupdate = $request->all();

        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data ruang berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.ruang.index')->with($notification);
    }

    public function show($id)
    {
        $no=1;
        $data = Ruang::findOrFail($id);
        return view('admin.ruang.show', compact('data','no'));
    }

    public function destroy($id)
    {
        $data = Ruang::findOrFail($id);

        $notification = array(
            'message' => 'Data ruangan berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.ruang.index')->with($notification);
    }
}
