<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\Ruangan;
use App\Model\RusakRuangan;
use Illuminate\Http\Request;

class RusakRuanganController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = RusakRuangan::orderBy('id', 'DESC')->get();
        return view('admin.rusakruangan.data.index', compact('data', 'no'));
    }

    public function status($id)
    {
        $data = RusakRuangan::findOrFail($id);
        $dataup['status'] = 'Sudah diperbaiki';
        $data->update($dataup);

        $bd = Ruangan::where('id', $data->ruangan_id)->first();
        $bdup['jumlah'] = $data->jumlah + $bd->jumlah;
        $bdup['jumlah_rusak'] = $bd->jumlah_rusak - $data->jumlah;
        $bd->update($bdup);

        $notification = array(
            'message' => 'Barang Rusak berhasil diperbaiki.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function show($id)
    {
        $data = RusakRuangan::findOrFail($id);
        return view('admin.rusakruangan.data.show', compact('data'));
    }
}
