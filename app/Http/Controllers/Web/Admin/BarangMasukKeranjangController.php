<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\BarangMasukRequest;
use App\Model\Barang;
use App\Model\BarangDetail;
use App\Model\BarangMasuk;
use App\Model\BarangMasukKeranjang;
use Illuminate\Support\Str;
use DB;
use Illuminate\Http\Request;

class BarangMasukKeranjangController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = BarangMasukKeranjang::orderBy('tanggal_masuk', 'DESC')->get()->all();
        return view('admin.barangmasuk.keranjang.index', compact('data', 'no'));
    }

    public function show($id)
    {
        $data = BarangMasukKeranjang::findOrFail($id);
        return view('admin.barangmasuk.keranjang.show', compact('data'));
    }

    public function create()
    {
        $barang = Barang::orderBy('barang', 'ASC')->get()->all();
        return view('admin.barangmasuk.keranjang.create', compact('barang'));
    }

    public function store(BarangMasukRequest $request)
    {

        $save = $request->all();
        $save['kode_barang'] = Str::random(10);

        BarangMasukKeranjang::create($save);

        $notification = array(
            'message' => 'Data barang masuk berhasil ditambah ke keranjang.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.keranjangbarangmasuk.index')->with($notification);
    }

    public function edit($id)
    {
        $barang = Barang::orderBy('barang', 'ASC')->get()->all();
        $data = BarangMasukKeranjang::findOrFail($id);
        return view('admin.barangmasuk.keranjang.edit', compact('data', 'barang'));
    }

    public function update(BarangMasukRequest $request, $id)
    {
        $data = BarangMasukKeranjang::findOrFail($id);

        $dataupdate = $request->all();
        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data barang masuk dikeranjang berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.keranjangbarangmasuk.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = BarangMasukKeranjang::findOrFail($id);

        $notification = array(
            'message' => 'Data barang masuk dikeranjang berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.keranjangbarangmasuk.index')->with($notification);
    }

    public function input()
    {

        $select = DB::table('barang_masuk_keranjangs')->get();

        foreach ($select as $s) {
            BarangMasuk::create([
                'barang_id' =>  $s->barang_id,
                'kode_barang' => $s->kode_barang,
                'jumlah' => $s->jumlah,
                'harga_satuan' => $s->harga_satuan,
                'harga_total' => $s->harga_total,
                'merk' => $s->merk,
                'sumber_dana' => $s->sumber_dana,
                'tanggal_masuk' => $s->tanggal_masuk,
                'keterangan' => $s->keterangan
            ]);
        }

        foreach ($select as $s) {
            BarangDetail::create([
                'barang_id' =>  $s->barang_id,
                'kode_barang' => $s->kode_barang,
                'jumlah' => $s->jumlah,
                'jumlah_rusak' => 0,
                'harga_satuan' => $s->harga_satuan,
                'harga_total' => $s->harga_total,
                'merk' => $s->merk,
                'sumber_dana' => $s->sumber_dana,
                'tanggal_masuk' => $s->tanggal_masuk,
                'keterangan' => $s->keterangan
            ]);
        }

        foreach ($select as $s) {
            DB::table('barang_masuk_keranjangs')->truncate([
                'barang_id' =>  $s->barang_id,
                'kode_barang' => $s->kode_barang,
                'jumlah' => $s->jumlah,
                'harga_satuan' => $s->harga_satuan,
                'harga_total' => $s->harga_total,
                'merk' => $s->merk,
                'sumber_dana' => $s->sumber_dana,
                'tanggal_masuk' => $s->tanggal_masuk,
                'keterangan' => $s->keterangan
            ]);
        }

        $notification = array(
            'message' => 'Data barang masuk dikeranjang berhasil dimasukkan.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);;
    }
}
