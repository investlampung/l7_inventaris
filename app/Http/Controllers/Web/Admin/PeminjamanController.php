<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\BarangDetail;
use App\Model\Peminjaman;
use Illuminate\Http\Request;

class PeminjamanController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Peminjaman::orderBy('id', 'DESC')->get()->all();
        return view('admin.peminjaman.data.index', compact('data', 'no'));
    }

    public function status($id)
    {
        $data = Peminjaman::findOrFail($id);
        $dataup['status'] = 'Sudah dikembalikan';
        $data->update($dataup);

        $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
        $bdup['jumlah'] = $data->jumlah_pinjam + $bd->jumlah;
        $bd->update($bdup);

        $notification = array(
            'message' => 'Barang peminjaman berhasil dikembalikan.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);
    }

    public function show($id)
    {
        $data = Peminjaman::findOrFail($id);
        return view('admin.peminjaman.data.show', compact('data'));
    }
}
