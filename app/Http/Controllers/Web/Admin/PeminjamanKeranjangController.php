<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Model\Barang;
use App\Model\BarangDetail;
use App\Model\Peminjaman;
use App\Model\PeminjamanKeranjang;
use App\User;
use DB;
use Illuminate\Http\Request;

class PeminjamanKeranjangController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $users = User::where('role', 'anggota')->where('hapus', 0)->orderBy('name', 'ASC')->get()->all();
        $data = PeminjamanKeranjang::orderBy('id', 'DESC')->get()->all();
        return view('admin.peminjaman.keranjang.index', compact('data', 'no', 'users'));
    }

    public function create()
    {
        $barang = Barang::orderBy('barang', 'ASC')->get()->all();
        $users = User::orderBy('name', 'ASC')->get()->all();
        return view('admin.peminjaman.keranjang.create', compact('barang', 'users'));
    }

    public function store(Request $request)
    {

        for ($a = 0; $a < count($request->barang_id); $a++) {
            $pk = BarangDetail::where('id', $request->barang_id[$a])->get()->first();

            if ($pk->jumlah >= $request->jumlah[$a]) {
                PeminjamanKeranjang::create([
                    'user_id' =>  $request->user_id,
                    'barang_detail_id' =>  $request->barang_id[$a],
                    'jumlah_pinjam' => $request->jumlah[$a],
                    'tanggal_pinjam' => $request->tanggal_pinjam,
                    'tanggal_kembali' => $request->tanggal_kembali,
                    'status' => 'Menunggu Persetujuan',
                ]);

                $bd = BarangDetail::where('id', $request->barang_id[$a])->get()->first();
                $bdupdate['jumlah'] = $bd->jumlah - $request->jumlah[$a];
                $bd->update($bdupdate);

                $notification = array(
                    'message' => 'Data peminjaman berhasil ditambah ke keranjang.',
                    'alert-type' => 'success'
                );
            } else {
                $notification = array(
                    'message' => 'Stok barang tidak memadai.',
                    'alert-type' => 'warning'
                );
            }
        }

        return redirect()->route('admin.keranjangpeminjaman.index')->with($notification);
    }

    public function tampil(Request $request)
    {
        $tanggal_pinjam = $request->tanggal_pinjam;
        $tanggal_kembali = $request->tanggal_kembali;
        $user_id = $request->user_id;

        $barang = Barang::orderBy('barang', 'ASC')->get()->all();
        $users = User::where('id', $user_id)->get()->first();

        return view('admin.peminjaman.keranjang.create', compact('barang', 'users', 'user_id', 'tanggal_pinjam', 'tanggal_kembali'));
    }

    public function edit($id)
    {
        $data = PeminjamanKeranjang::findOrFail($id);
        return view('admin.peminjaman.keranjang.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {

        $data = PeminjamanKeranjang::findOrFail($id);

        $dataupdate = $request->all();

        if ($data->jumlah_pinjam == $request->jumlah_pinjam) {
            $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
            $bdupdate['jumlah'] = $bd->jumlah;
            $bd->update($bdupdate);
        } else if ($data->jumlah_pinjam > $request->jumlah_pinjam) {
            $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
            $kurang = $data->jumlah_pinjam - $request->jumlah_pinjam;
            $bdupdate['jumlah'] = $bd->jumlah + $kurang;
            $bd->update($bdupdate);
        } else {
            $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
            $lebih = $request->jumlah_pinjam - $data->jumlah_pinjam;
            $bdupdate['jumlah'] = $bd->jumlah - $lebih;
            $bd->update($bdupdate);
        }

        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data peminjaman berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.keranjangpeminjaman.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = PeminjamanKeranjang::findOrFail($id);

        $bd = BarangDetail::where('id', $data->barang_detail_id)->get()->first();
        $bdupdate['jumlah'] = $bd->jumlah + $data->jumlah_pinjam;
        $bd->update($bdupdate);

        $notification = array(
            'message' => 'Data peminjaman dikeranjang berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.keranjangpeminjaman.index')->with($notification);
    }

    public function input()
    {

        $select = PeminjamanKeranjang::where('status', 'Telah disetujui')->get()->all();

        foreach ($select as $s) {
            Peminjaman::create([
                'user_id' =>  $s->user_id,
                'barang_detail_id' =>  $s->barang_detail_id,
                'jumlah_pinjam' =>  $s->jumlah_pinjam,
                'tanggal_pinjam' =>  $s->tanggal_pinjam,
                'tanggal_kembali' =>  $s->tanggal_kembali,
                'status' => 'Belum dikembalikan'
            ]);
        }

        foreach ($select as $s) {
            $data = PeminjamanKeranjang::findOrFail($s->id);
            $data->delete();
        }

        $notification = array(
            'message' => 'Data peminjaman dikeranjang berhasil dimasukkan.',
            'alert-type' => 'success'
        );

        return redirect()->back()->with($notification);;
    }
}
