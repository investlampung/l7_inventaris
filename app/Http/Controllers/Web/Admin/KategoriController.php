<?php

namespace App\Http\Controllers\Web\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\KategoriRequest;
use App\Model\Kategori;
use Illuminate\Http\Request;

class KategoriController extends Controller
{
    public function __construct()
    {
        $this->middleware(['auth', 'verified']);
        $this->middleware('role:admin');
    }

    public function index()
    {
        $no = 1;
        $data = Kategori::orderBy('kategori', 'ASC')->get()->all();
        return view('admin.kategori.index', compact('data', 'no'));
    }

    public function create()
    {
        return view('admin.kategori.create');
    }

    public function store(KategoriRequest $request)
    {

        // SIMPAN DATA SISWA META
        $save = $request->all();

        Kategori::create($save);

        $notification = array(
            'message' => 'Data Kategori berhasil ditambah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.kategori.index')->with($notification);
    }

    public function edit($id)
    {
        $data = Kategori::findOrFail($id);
        return view('admin.kategori.edit', compact('data'));
    }

    public function update(Request $request, $id)
    {
        $data = Kategori::findOrFail($id);

        $dataupdate = $request->all();

        $data->update($dataupdate);

        $notification = array(
            'message' => 'Data Kategori berhasil diubah.',
            'alert-type' => 'success'
        );

        return redirect()->route('admin.kategori.index')->with($notification);
    }

    public function destroy($id)
    {
        $data = Kategori::findOrFail($id);

        $notification = array(
            'message' => 'Data kategori berhasil dihapus.',
            'alert-type' => 'error'
        );

        $data->delete();

        return redirect()->route('admin.kategori.index')->with($notification);
    }
}
