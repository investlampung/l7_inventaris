<?php

namespace App;

use App\Model\Keluar;
use App\Model\KeluarKeranjang;
use App\Model\Peminjaman;
use App\Model\PeminjamanKeranjang;
use App\Model\Ruang;
use App\Model\RusakRuangan;
use App\Model\RusakRuanganKeranjang;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'no_induk', 'jk', 'alamat', 'status', 'no_telp', 'photo', 'role', 'hapus'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function ruang()
    {
        return $this->hasMany(Ruang::class);
    }

    public function peminjaman()
    {
        return $this->hasMany(Peminjaman::class);
    }

    public function peminjaman_keranjang()
    {
        return $this->hasMany(PeminjamanKeranjang::class);
    }

    public function keluar()
    {
        return $this->hasMany(Keluar::class);
    }

    public function keluar_keranjang()
    {
        return $this->hasMany(KeluarKeranjang::class);
    }

    public function rusak_ruangan()
    {
        return $this->hasMany(RusakRuangan::class);
    }

    public function rusak_ruangan_keranjang()
    {
        return $this->hasMany(RusakRuanganKeranjang::class);
    }
}
