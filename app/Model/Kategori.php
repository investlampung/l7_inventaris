<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Kategori extends Model
{
    protected $guarded=[];

    public function barang()
    {
        return $this->hasMany(Barang::class);
    }
}
