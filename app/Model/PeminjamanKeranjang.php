<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class PeminjamanKeranjang extends Model
{
    protected $guarded = [];
    protected $dates = ['tanggal_pinjam', 'tanggal_kembali'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function barang_detail()
    {
        return $this->belongsTo(BarangDetail::class);
    }
}
