<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class RusakRuangan extends Model
{
    protected $guarded = [];
    protected $dates = ['tanggal_rusak'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ruang()
    {
        return $this->belongsTo(Ruang::class);
    }

    public function ruangan()
    {
        return $this->belongsTo(Ruangan::class);
    }

    public function barang_detail()
    {
        return $this->belongsTo(BarangDetail::class);
    }
}
