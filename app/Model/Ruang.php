<?php

namespace App\Model;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Ruang extends Model
{
    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ruangan()
    {
        return $this->hasMany(Ruangan::class);
    }

    public function ruangan_detail()
    {
        return $this->hasMany(RuanganDetail::class);
    }

    public function ruangan_keranjang()
    {
        return $this->hasMany(RuanganKeranjang::class);
    }

    public function rusak_ruangan()
    {
        return $this->hasMany(RusakRuangan::class);
    }

    public function rusak_ruangan_keranjang()
    {
        return $this->hasMany(RusakRuanganKeranjang::class);
    }
}
