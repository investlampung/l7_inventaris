<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BarangMasuk extends Model
{
    protected $guarded = [];
    protected $dates = ['tanggal_masuk'];
    public function barang()
    {
        return $this->belongsTo(Barang::class);
    }
}
