<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class BarangDetail extends Model
{
    protected $guarded = [];
    protected $dates = ['tanggal_masuk'];
    public function barang()
    {
        return $this->belongsTo(Barang::class);
    }

    public function peminjaman()
    {
        return $this->hasMany(Peminjaman::class);
    }

    public function peminjaman_keranjang()
    {
        return $this->hasMany(PeminjamanKeranjang::class);
    }

    public function ruangan()
    {
        return $this->hasMany(Ruangan::class);
    }

    public function ruangan_keranjang()
    {
        return $this->hasMany(RuanganKeranjang::class);
    }

    public function keluar()
    {
        return $this->hasMany(Keluar::class);
    }

    public function keluar_keranjang()
    {
        return $this->hasMany(KeluarKeranjang::class);
    }

    public function rusak_ruangan()
    {
        return $this->hasMany(RusakRuangan::class);
    }

    public function rusak_ruangan_keranjang()
    {
        return $this->hasMany(RusakRuanganKeranjang::class);
    }
}
