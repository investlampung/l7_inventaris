<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $guarded = [];

    public function kategori()
    {
        return $this->belongsTo(Kategori::class);
    }

    public function barang_masuk()
    {
        return $this->hasMany(BarangMasuk::class);
    }

    public function barang_masuk_keranjang()
    {
        return $this->hasMany(BarangMasukKeranjang::class);
    }

    public function barang_detail()
    {
        return $this->hasMany(BarangDetail::class);
    }

}
