<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    protected $guarded = [];

    public function ruang()
    {
        return $this->belongsTo(Ruang::class);
    }

    public function barang_detail()
    {
        return $this->belongsTo(BarangDetail::class);
    }

    public function rusak_ruangan()
    {
        return $this->hasMany(RusakRuangan::class);
    }

    public function rusak_ruangan_keranjang()
    {
        return $this->hasMany(RusakRuanganKeranjang::class);
    }
}
