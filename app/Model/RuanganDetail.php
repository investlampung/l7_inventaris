<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class RuanganDetail extends Model
{
    protected $guarded = [];
    protected $dates = ['tanggal_masuk'];

    public function ruang()
    {
        return $this->belongsTo(Ruang::class);
    }

    public function barang_detail()
    {
        return $this->belongsTo(BarangDetail::class);
    }
}
