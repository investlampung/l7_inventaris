<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    public function run()
    {
        App\User::create([
            'email'         => 'admin@smkpelitapesawaran.sch.id',
            'name'          => 'Administrator',
            'password'      => bcrypt('1sampai9'),
            'no_induk'      => '1',
            'alamat'        => '',
            'no_telp'       => '',
            'jk'            => 'Laki-Laki',
            'photo'         => 'avatar4.png',
            'status'        => '',
            'role'          => 'admin',
            'hapus'          => 0,
        ]);
        App\User::create([
            'email'         => 'sarpras@smkpelitapesawaran.sch.id',
            'name'          => 'Waka SarPras',
            'password'      => bcrypt('1sampai9'),
            'no_induk'      => '2',
            'alamat'        => '',
            'no_telp'       => '',
            'jk'            => 'Laki-Laki',
            'photo'         => 'avatar5.png',
            'status'        => '',
            'role'          => 'sarpras',
            'hapus'          => 0,
        ]);
        App\User::create([
            'email'         => 'erwin@smkpelitapesawaran.sch.id',
            'name'          => 'Erwin Dianto',
            'password'      => bcrypt('1sampai9'),
            'no_induk'      => '111111',
            'alamat'        => 'Krandegan',
            'no_telp'       => '089631073926',
            'jk'            => 'Laki-Laki',
            'photo'         => '-',
            'status'        => 'Guru',
            'role'          => 'anggota',
            'hapus'          => 0,
        ]);
        App\User::create([
            'email'         => 'rino@smkpelitapesawaran.sch.id',
            'name'          => 'Rino Setiawan',
            'password'      => bcrypt('1sampai9'),
            'no_induk'      => '211111',
            'alamat'        => 'Sukaraja',
            'no_telp'       => '089631876546',
            'jk'            => 'Laki-Laki',
            'photo'         => '-',
            'status'        => 'Karyawan',
            'role'          => 'anggota',
            'hapus'          => 0,
        ]);
    }
}
