<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRuanganKeranjangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ruangan_keranjangs', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('ruang_id');
            $table->bigInteger('barang_detail_id');
            $table->integer('jumlah');
            $table->date('tanggal_masuk');
            $table->string('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ruangan_keranjangs');
    }
}
